﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Inmobiliaria.Core.Common
{
    public class Util
    {
        public static string EncodePassword(string password)
        {
            var md5 = new MD5CryptoServiceProvider();
            var originalBytes = Encoding.Default.GetBytes(password);
            var encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes);
        }
    }
}
