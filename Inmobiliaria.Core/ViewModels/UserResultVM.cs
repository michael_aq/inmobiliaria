﻿namespace Inmobiliaria.Core.ViewModels
{
    public class UserResultVM
    {
        public int CodigoTrabajador { get; set; }
        public string NombreTrabajador { get; set; }
        public string CodUserName { get; set; }
        public string Contrasenia { get; set; }
        public bool EsUsuarioAD { get; set; }
        public string ReturnUrl { get; set; }
        public int Rol { get; set; }
    }
}
