﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
    public class ReservaVM : AuditoriaBaseVM
    {
        public int IdReserva { get; set; }
        public int IdDepartamento { get; set; }
        [Display(Name = "Departamento")]
        public string NombreDepartamento { get; set; }
        public int IdVendedor { get; set; }
        [Display(Name = "Vendedor")]
        public string NombreVendedor { get; set; }
        public int IdCliente { get; set; }
        [Display(Name = "Cliente")]
        public string NombreCliente { get; set; }
        [Required]
        [Display(Name = "Valor")]
        [Range(1000, int.MaxValue, ErrorMessage = "Valor debe ser mayor a 1000")]
        public decimal ValorReserva { get; set; }
        [Display(Name = "Fecha Limite")]
        public DateTime FechaLimite { get; set; }
        public char Estado { get; set; }
        public string DescripcionEstado { get; set; }

    }
}
