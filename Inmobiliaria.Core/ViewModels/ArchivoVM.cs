﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
    public class ArchivoVM : AuditoriaBaseVM
    {
        public int IdArchivo { get; set; }
        public int IdGuia { get; set; }
        [Display(Name = "Codigo")]
        public Guid? Codigo { get; set; }
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
        public char Estado { get; set; }

    }
}
