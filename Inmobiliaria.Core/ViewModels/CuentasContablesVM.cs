﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.ViewModels.Base;
using System.ComponentModel.DataAnnotations;


namespace Inmobiliaria.Core.ViewModels
{
    public class CuentasContablesVM : AuditoriaBaseVM
    {
        [Display(Name = "Cuenta contable")] public string CTA_CtaCon { get; set; }
        [Display(Name = "Descripción")] public string CTA_Des { get; set; }
        [Display(Name = "Estado del Registro Físico del Archivo")] public string CTA_EstRegA { get; set; }

    }
}
