﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.ViewModels.Base;
using System.ComponentModel.DataAnnotations;


namespace Inmobiliaria.Core.ViewModels
{
    public class ReembolsoVM : AuditoriaBaseVM

    {
        [Display(Name = "Código de pago_CAB")] public int REM_Cod { get; set; }
        [Display(Name = "Importe de reposición")] public double REM_Imp { get; set; }
        [Display(Name = "Cuenta corriente en")] public string REM_CtaCte { get; set; }
        [Display(Name = "Fecha de envío")] public DateTime REM_FecEnv { get; set; }
        [Display(Name = "Fecha de contabilización")] public DateTime REM_FecCon { get; set; }
        [Display(Name = "Estado")] public string REM_EstReg { get; set; }
        [Display(Name = "Código de rendición")] public int IdRendicion { get; set; }
        [Display(Name = "Cuenta contable")] public string FF_CtaCon { get; set; }
        [Display(Name = "División")] public int FF_Div { get; set; }
        public string FF_EstReg { get; set; }
        public string FF_EstRegCtaCon { get; set; }
        [Display(Name = "Código del fondo fijo")] public int FF_Cod { get; set; }
        public string RENC_EstReg { get; set; }

        public Nullable<bool> REM_APRO { get; set; }

    }
}
