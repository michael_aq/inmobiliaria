﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Inmobiliaria.Core.ViewModels
{
    public class RendicionDETVM : AuditoriaBaseVM

    {
        [Display(Name = "Código")] public int REND_Cod { get; set; }
        [Display(Name = "Código de división")] public int DIV_Cod { get; set; }//de cabecera
        [Display(Name = "Fecha de contabilización")] public DateTime REND_FecCon { get; set; }
        [Display(Name = "Fecha de documento")] public DateTime REND_FecDoc { get; set; }
        [Display(Name = "Tipo de documento")] public int REND_TipDoc { get; set; }
        [Display(Name = "Referencia")] public string REND_DocRef { get; set; }
        [Display(Name = "Tipo de impuesto")] public string TIPIMP_TipImp { get; set; }
        [Display(Name = "Monto del gasto")] public double REND_MonGas { get; set; }
        [Display(Name = "Descripción")] public string REND_Des { get; set; }
        [Display(Name = "Tipo orden")] public string TIPORD_TipOrd { get; set; }
        [Display(Name = "Cuenta contable asociada")] public string REND_CtaCon { get; set; }
        [Display(Name = "Estado Registro")] public string REND_EstRegCtaCon { get; set; }
        //[Display(Name = "Estado Registro")] public string TIPORD_EstReg { get; set; }
        [Display(Name = "Código de rendición")] public int IdRendicion { get; set; }
        [Display(Name = "Cuenta contable")] public string FF_CtaCon { get; set; }
        [Display(Name = "División")] public int FF_Div { get; set; }
        //public string FF_EstReg { get; set; }
        //public string FF_EstRegCtaCon { get; set; }
        [Display(Name = "Código del fondo fijo")] public int FF_Cod { get; set; }
        [Display(Name = "Código del proveedor")] public int PRO_Cod { get; set; }
        //public string RENC_EstReg { get; set; }

        public Nullable<bool> REND_OBSE_MODI { get; set; }
    }
}
