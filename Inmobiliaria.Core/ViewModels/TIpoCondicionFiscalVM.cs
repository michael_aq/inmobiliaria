﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.ViewModels.Base;
using System.ComponentModel.DataAnnotations;



namespace Inmobiliaria.Core.ViewModels
{
    public class TipoCondicionFiscalVM 
    {
        [Display(Name = "Tipo de condición Fiscal")] public string TIPCONFIS_TipConFis { get; set; }
        [Display(Name = "Descripción")] public string TIPCONFIS_Des { get; set; }
        [Display(Name = "Estado Registro")] public string TIPCONFIS_EstReg { get; set; }

    }
}
