﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.ViewModels.Base;
using System.ComponentModel.DataAnnotations;


namespace Inmobiliaria.Core.ViewModels
{
    public class TipoImpuestoVM
    {
        [Display(Name = "Tipo de impuesto")] public string TIPIMP_TipImp { get; set; }
        [Display(Name = "Descripción")] public string TIPIMP_Des { get; set; }
        [Display(Name = "Porcentaje asociado")] public double TIPIMP_Por { get; set; }

    }
}
