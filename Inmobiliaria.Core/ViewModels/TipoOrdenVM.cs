﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Inmobiliaria.Core.ViewModels.Base;
using System.ComponentModel.DataAnnotations;


namespace Inmobiliaria.Core.ViewModels
{
    public class TipoOrdenVM
    {
        [Display(Name = "Tipo orden")] public string TIPORD_TipOrd { get; set; }
        [Display(Name = "Descripción")] public string TIPORD_Des { get; set; }
        [Display(Name = "Estado Registro")] public string TIPORD_EstReg { get; set; }

    }
}
