﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.ViewModels.Base;
using System.ComponentModel.DataAnnotations;


namespace Inmobiliaria.Core.ViewModels
{
    public class EstadoRegistroVM
    {
        [Display(Name = "Estado de Registro")] public string ESTREG_EstReg { get; set; }
        [Display(Name = "Descripción")] public string ESTREG_Des { get; set; }

    }
}
