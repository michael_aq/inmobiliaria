﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
    public class ClienteVM : AuditoriaBaseVM
    {
        public int IdCliente { get; set; }
        [Display (Name="Nombre")]
        public string NombreCliente { get; set; }
        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }
        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }
        [Display(Name = "Direccion")]
        public string Direccion { get; set; }
        [Display(Name = "DNI")]
        public string DNI { get; set; }
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }
        [Display(Name = "Correo Electronico")]
        public string CorreoElectronico { get; set; }
        public char? Estado { get; set; }
        public string DescripcionEstado { get; set; }

    }
}
