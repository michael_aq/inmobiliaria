﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
    public class CondominioVM : AuditoriaBaseVM
    {
 
        public int IdCondominio { get; set; }
        [Display(Name = "Direccion")]
        public string CondominioDireccion { get; set; }
        [Display(Name = "Nombre")]
        public string NombreCondominio { get; set; }
        [Display(Name = "Ruta")]
        public string CondominioRuta { get; set; }//CREO SE REFIERE A ARCHIVOS
        [Display(Name = "Ubicacion")]
        public string Ubicacion { get; set; }
        public char Estado { get; set; }
        public string DescripcionEstado { get; set; }

    }
}
