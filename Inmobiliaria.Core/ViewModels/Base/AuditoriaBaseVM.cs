﻿using System;

namespace Inmobiliaria.Core.ViewModels.Base
{
    public class AuditoriaBaseVM
    {
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; } 
        public string UsuarioModi { get; set; }
        public DateTime? FechaModi { get; set;}
    }
}
