﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
    public class DepartamentoVM : AuditoriaBaseVM
    {
        public int IdDepartamento { get; set; }
        public int IdCondominio { get; set; }
        public string NombreCondominio { get; set; }
        [Display(Name = "Nombre")]
        public string NombreDepartamento { get; set; }
        [Display(Name = "Direccion")]
        public string Direccion { get; set; }
        [Display(Name = "Codigo")]
        public string DepartamentoCodigo { get; set; }
        public char Estado { get; set; }
        public string DescripcionEstado { get; set; }

    }
}
