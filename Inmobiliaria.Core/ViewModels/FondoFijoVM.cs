﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace Inmobiliaria.Core.ViewModels
{
    public class FondoFijoVM : AuditoriaBaseVM
    {
        [Display(Name = "Código del fondo fijo")] public int FF_Cod { get; set; }
        [Display(Name = "Cuenta corriente en bancos")] public string FF_CtaCte { get; set; }
        [Display(Name = "Cuenta contable")] public string FF_CtaCon { get; set; }
        [Display(Name = "Importe fijo")] public double FF_imp { get; set; }
        [Display(Name = "División")] public int FF_Div { get; set; }
        [Display(Name = "Estado del Registro Fondo")] public string FF_EstReg { get; set; }
        [Display(Name = "Estado del Registro Cuenta")] public string FF_EstRegCtaCon { get; set; }
        [Display(Name = "Descripción")] public string FF_Des { get; set; }


    }
}
