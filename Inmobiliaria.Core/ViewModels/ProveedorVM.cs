﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
    public class ProveedorVM : AuditoriaBaseVM
    {
        [Display(Name = "Código de Proveedor")] public int PRO_Cod { get; set; }
        [Display(Name = "Nombre Proveedor")] public string PRO_Nom { get; set; }
        [Display(Name = "Ruc del proveedor")] public string PRO_Ruc { get; set; }
        [Display(Name = "Tipo de condición Fiscal")] public string TIPCONFIS_TipConFis { get; set; }
        [Display(Name = "Fecha de Inscripción Proveedor")] public DateTime PRO_FecIns { get; set; }
        [Display(Name = "Cuenta bancara asociada")] public string PRO_CtaCte { get; set; }
        [Display(Name = "Direccion Proveedor")] public string PRO_Dir { get; set; }
        [Display(Name = "Domicilio fiscal")] public string PRO_DomFis { get; set; }
        [Display(Name = "Estado del Registro Físico del Archivo")] public string PRO_EstReg { get; set; }
        [Display(Name = "Estado Registro")] public string TIPCONFIS_EstReg { get; set; }


    }
}
