﻿using Inmobiliaria.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.ViewModels
{
   public class DivisionVM : AuditoriaBaseVM
    {
        [Display(Name = "Código de división")] public int DIV_Codigo { get; set; }
        [Display(Name = "Descripción")] public string DIV_Descripcion { get; set; }
        [Display(Name = "Fecha de inicio")] public DateTime DIV_FecInicio { get; set; }
        [Display(Name = "Tiempo de duración en meses")] public int DIV_TieDuracion { get; set; }
        [Display(Name = "Ruta de division")] public string DIV_Ruta { get; set; }
        [Display(Name = "Direccion")] public string DIV_Direccion { get; set; }
        [Display(Name = "Ubigeo")] public string DIV_Ubigeo { get; set; }     
        [Display(Name = "Estado del Registro Físico del Archivo")] public string DIV_EstRegistro { get; set; }

    }
}
