﻿using Inmobiliaria.Core.ViewModels;
using System.Collections.Generic;

namespace Inmobiliaria.Core.IRepositories
{
    public interface IUserRepository
    {
        ICollection<UserResultVM> GetUserByUserName(string userName);
        IList<string> GetRolesByUser(string userCode);
        bool ValidateUserPassword(string userName, string encodePass);
        int UpdateWorker(string pass);
        //ICollection<UserResultVM> GetAllUsers();
        //int? InsertUser(UserResultVM model, string user);
        //int? UpdateUser(UserResultVM model, string user);
    }
}
