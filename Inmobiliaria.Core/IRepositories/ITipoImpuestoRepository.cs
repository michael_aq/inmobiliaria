﻿using Inmobiliaria.Core.IRepositories.Common;
using Inmobiliaria.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.IRepositories
{
    public interface ITipoImpuestoRepository : IRepository4<TipoImpuestoVM>
    {
    }
}
