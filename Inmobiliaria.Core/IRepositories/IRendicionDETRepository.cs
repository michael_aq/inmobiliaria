﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.IRepositories.Common;
using Inmobiliaria.Core.ViewModels;

namespace Inmobiliaria.Core.IRepositories
{
    public interface IRendicionDETRepository: IRepository<RendicionDETVM>
    {
        ICollection<RendicionDETVM> GetAllByCabecera(int id);
        double? eliminacion(int ID);
        double? actualizacion(RendicionDETVM model);
    }
}
