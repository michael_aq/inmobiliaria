﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inmobiliaria.Core.IRepositories.Common;
using Inmobiliaria.Core.ViewModels;

namespace Inmobiliaria.Core.IRepositories
{
    public interface IReembolsoRepository : IRepository<ReembolsoVM>
    {
        void Reeditar(int idRendicion);
        void Reembolsar(int idReembolso);
    }
}
