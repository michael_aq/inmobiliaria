﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Inmobiliaria.Core.IRepositories.Common
{
    // Repositorio general para las tablas referenciales y generales con ID int
    public interface IRepository3<T>
    {
        ICollection<T> GetAll();
        T GetByID(int id);
        
    }
}
