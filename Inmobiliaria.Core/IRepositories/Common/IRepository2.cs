﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.IRepositories.Common
{
    // Repositorio general para los maestros con ID String
    public interface IRepository2<T>
    {
        ICollection<T> GetAll();
        T GetByID(String id);
        int update(T model);
        int Insert(T model);
        int Delete(String model);

    }
}
