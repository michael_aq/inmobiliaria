﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Inmobiliaria.Core.IRepositories.Common
{
    // Repositorio general para las tablas referenciales y generales con ID String
    public interface IRepository4<T>
    {
        ICollection<T> GetAll();
        T GetByID(string id);

    }
}
