﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Core.IRepositories.Common
{
    // Repositorio general para los maestros con ID int
    public interface IRepository<T>
    {
        ICollection<T> GetAll();
        T GetByID(int id);
        int update(T model);
        int Insert(T model);
        int Delete(int model);
        
    }
}
