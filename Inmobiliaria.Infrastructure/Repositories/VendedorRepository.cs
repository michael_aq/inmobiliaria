﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class VendedorRepository : IVendedorRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public VendedorRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }
        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_TM_VEND_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<VendedorVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<VendedorVM>>(_context.TS_TM_VEND_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public VendedorVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(VendedorVM model)
        {
            try
            {
                int? result = _context.TS_TM_VEND_I01(model.NombreVendedor
                    , model.ApellidoPaterno
                    , model.ApellidoMaterno
                    , model.Direccion
                    , model.DNI
                    , model.Telefono
                    //, model.CorreoElectronico
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(VendedorVM model)
        {
            try
            {
                int? result = _context.TS_TM_VEND_U01(model.IdVendedor,
                    model.NombreVendedor
                    , model.ApellidoPaterno
                    , model.ApellidoMaterno
                    , model.Direccion
                    , model.DNI
                    , model.Telefono
                    //, model.CorreoElectronico
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
