﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class DivisionRepository : IDivisionRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public DivisionRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_GZM_Division_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public ICollection<DivisionVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<DivisionVM>>(_context.TS_GZM_Division_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public DivisionVM GetByID(int Id)
        {
            throw new NotImplementedException();

        }

        public int Insert(DivisionVM model)
        {
            try
            {
                int? result = _context.TS_GZM_Division_I01(
                    model.DIV_Descripcion
                    , model.DIV_Ruta
                    , model.DIV_Direccion
                    , model.DIV_Ubigeo
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(DivisionVM model)
        {
            try
            {
                int? result = _context.TS_GZM_Division_U01(
                    model.DIV_Codigo
                    , model.DIV_Direccion
                    , model.DIV_Descripcion
                    , model.DIV_Ruta
                    , model.DIV_Ubigeo
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
