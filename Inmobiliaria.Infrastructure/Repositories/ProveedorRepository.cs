﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class ProveedorRepository : IProveedorRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public ProveedorRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
        }

        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_P0M_Proveedor_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<ProveedorVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<ProveedorVM>>(_context.TS_P0M_Proveedor_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public ProveedorVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(ProveedorVM model)
        {
            try
            {
                int? result = _context.TS_P0M_Proveedor_I01(
                    model.PRO_Cod
                   , model.PRO_Nom
                   , model.PRO_Ruc
                   , model.TIPCONFIS_TipConFis
                   , model.PRO_FecIns
                   , model.PRO_CtaCte
                   , model.PRO_Dir
                   , model.PRO_DomFis
                   //, model.PRO_EstReg
                   //, model.TIPCONFIS_EstReg
                    , model.UsuarioCrea).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(ProveedorVM model)
        {
            try
            {
                int? result = _context.TS_P0M_Proveedor_U01(
                    model.PRO_Cod
                   , model.PRO_Nom
                   , model.PRO_Ruc
                   , model.TIPCONFIS_TipConFis
                   ,model.PRO_FecIns
                   , model.PRO_CtaCte
                   , model.PRO_Dir
                   , model.PRO_DomFis
                   , model.PRO_EstReg
                   , model.UsuarioModi);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
