﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class FondoFijoRepository: IFondoFijoRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public FondoFijoRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_F1M_Fondo_fijo_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<FondoFijoVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<FondoFijoVM>>(_context.TS_F1M_Fondo_fijo_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public FondoFijoVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(FondoFijoVM model)
        {
            try
            {
                int? result = _context.TS_F1M_Fondo_fijo_I01(
                     model.FF_CtaCte
                    , model.FF_CtaCon
                    , model.FF_imp
                    , model.FF_Div
                    , model.FF_Des
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(FondoFijoVM model)
        {
            try
            {
                int? result = _context.TS_F1M_Fondo_fijo_U01(
                    model.FF_Cod
                    , model.FF_CtaCte
                    , model.FF_CtaCon
                    , model.FF_imp
                    , model.FF_Div
                    , model.FF_EstReg
                    ,model.FF_EstRegCtaCon
                    , model.FF_Des
                    , model.UsuarioModi);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
