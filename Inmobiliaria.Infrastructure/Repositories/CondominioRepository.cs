﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class CondominioRepository : ICondominioRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public CondominioRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }
        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_TM_COND_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<CondominioVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<CondominioVM>>(_context.TS_TM_COND_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public CondominioVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(CondominioVM model)
        {
            try
            {
                int? result = _context.TS_TM_COND_I01(model.CondominioDireccion
                    , model.NombreCondominio
                    , model.CondominioRuta
                    , model.Ubicacion
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(CondominioVM model)
        {
            try
            {
                int? result = _context.TS_TM_COND_U01(model.IdCondominio
                    , model.CondominioDireccion
                    , model.NombreCondominio
                    , model.CondominioRuta
                    , model.Ubicacion
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
