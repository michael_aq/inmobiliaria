﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class EstadoRegistroRepository : IEstadoRegistroRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public EstadoRegistroRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public ICollection<EstadoRegistroVM> GetSegunTipo(string tipo)
        {
            var result = _mapper.Map<List<EstadoRegistroVM>>(_context.TS_GZZ_Estado_Registro_Q01(tipo));
            return result;
        }
    }
}
