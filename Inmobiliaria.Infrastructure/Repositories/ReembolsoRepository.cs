﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class ReembolsoRepository : IReembolsoRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public ReembolsoRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_F2T_Rembolso_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<ReembolsoVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<ReembolsoVM>>(_context.TS_F2T_Rembolso_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public ReembolsoVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(ReembolsoVM model)
        {
            try
            {
                int? result = _context.TS_F2T_Rembolso_I01(
                    model.REM_Imp
                    , model.REM_CtaCte
                    , model.REM_FecEnv
                    , model.REM_FecCon
                    //, model.FF_CtaCon
                    //, model.FF_Div
                    //, model.FF_Cod
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Reeditar(int idRendicion)
        {
            try
            {
                int? result = _context.TS_F2T_Rembolso_U02(idRendicion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Reembolsar(int idReembolso)
        {
            try
            {
                int? result = _context.TS_F2T_Rembolso_U02(idReembolso);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(ReembolsoVM model)
        {
            try
            {
                int? result = _context.TS_F2T_Rembolso_U01(
                    model.REM_Cod
                    , model.REM_Imp
                    , model.REM_CtaCte
                    , model.REM_FecEnv
                    , model.REM_FecCon
                    , model.REM_EstReg
                    , model.IdRendicion
                    //, model.FF_CtaCon
                    //, model.FF_Div
                    //, model.FF_Cod
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
