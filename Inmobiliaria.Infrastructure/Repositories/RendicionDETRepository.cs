﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class RendicionDETRepository : IRendicionDETRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public RendicionDETRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public double? actualizacion(RendicionDETVM model)
        {
            try
            {
                var result = _context.TS_F3T_Rendicion_DET_U01(
                     model.REND_Cod
                   , model.DIV_Cod
                   , model.REND_FecCon
                   , model.REND_FecDoc
                   , model.REND_TipDoc
                   , model.REND_DocRef
                   , model.TIPIMP_TipImp
                   , model.REND_MonGas
                   , model.REND_Des
                   , model.TIPORD_TipOrd
                   , model.REND_CtaCon
                   , model.IdRendicion
                   , model.FF_CtaCon
                   , model.FF_Div
                   , model.FF_Cod
                   , model.PRO_Cod
                   , _user).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int Delete(int model)
        {
            throw new NotImplementedException();

        }

        public double? eliminacion(int ID)
        {
            try
            {
                var result = _context.TS_F3T_Rendicion_DET_D01(ID).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<RendicionDETVM> GetAll()
        {
            throw new NotImplementedException();
        }

        public ICollection<RendicionDETVM> GetAllByCabecera(int id)
        {
            try
            {
                var result = _mapper.Map<List<RendicionDETVM>>(_context.TS_F3T_Rendicion_DET_Q01(id));
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public RendicionDETVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(RendicionDETVM model)
        {
            try
            {
                int? result = _context.TS_F3T_Rendicion_DET_I01( model.REND_FecCon
                   , model.DIV_Cod
                   , model.REND_FecDoc
                   , model.REND_TipDoc
                   , model.REND_DocRef
                   , model.TIPIMP_TipImp
                   , model.REND_MonGas
                   , model.REND_Des
                   , model.TIPORD_TipOrd
                   , model.REND_CtaCon
                   , model.IdRendicion
                   //, model.FF_CtaCon
                   //, model.FF_Div
                   //, model.FF_Cod
                   , model.PRO_Cod
                    , _user).FirstOrDefault();//.FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(RendicionDETVM model)
        {
            throw new NotImplementedException();
        }
    }
}
