﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class TipoOrdenRepository : ITipoOrdenRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public TipoOrdenRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        
        public ICollection<TipoOrdenVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<TipoOrdenVM>>(_context.TS_GZZ_Tipo_Orden_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public TipoOrdenVM GetByID(string Id)
        {
            throw new NotImplementedException();

        }

        
    }
}
