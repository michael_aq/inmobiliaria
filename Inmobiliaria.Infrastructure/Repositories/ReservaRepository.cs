﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class ReservaRepository : IReservaRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public ReservaRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }
        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_TR_RESE_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<ReservaVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<ReservaVM>>(_context.TS_TR_RESE_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public ReservaVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(ReservaVM model)
        {
            try
            {
                int? result = _context.TS_TR_RESE_I01(model.IdDepartamento
                    ,model.IdVendedor
                    ,model.IdCliente
                    ,model.ValorReserva
                    ,model.FechaLimite
                    ,_user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(ReservaVM model)
        {
            try
            {
                int? result = _context.TS_TR_RESE_U01(model.IdReserva
                    , model.IdDepartamento
                    , model.IdVendedor
                    , model.IdCliente
                    , model.ValorReserva
                    ,model.FechaLimite
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
