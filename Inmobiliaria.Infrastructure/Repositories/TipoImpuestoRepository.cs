﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class TipoImpuestoRepository : ITipoImpuestoRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public TipoImpuestoRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        
        public ICollection<TipoImpuestoVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<TipoImpuestoVM>>(_context.TS_GZZ_Tipo_Impuesto_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public TipoImpuestoVM GetByID(string Id)
        {
            throw new NotImplementedException();

        }

        
    }
}
