﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class RendicionCABRepository : IRendicionCABRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public RendicionCABRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_F3T_Rendicion_CAB_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<RendicionCABVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<RendicionCABVM>>(_context.TS_F3T_Rendicion_CAB_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public RendicionCABVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(RendicionCABVM model)
        {
            try
            {
                int? result = _context.TS_F3T_Rendicion_CAB_I01(
                    model.FechaCreacionRendicion
                   , model.FechaEnvioRendicion
                   , model.FF_Cod
                   //, model.FF_CtaCon
                   //, model.FF_Div
                   //, model.FF_EstReg
                   //, model.TotalRendicion
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(RendicionCABVM model)
        {
            try
            {
                int? result = _context.TS_F3T_Rendicion_CAB_U01(
                     model.IdRendicion
                   , model.FechaCreacionRendicion
                   , model.FechaEnvioRendicion
                   , model.FF_Cod
                   , model.FF_CtaCon
                   , model.FF_Div
                   , model.EstadoRegistroRendicion
                   , model.TotalRendicion
                   , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
