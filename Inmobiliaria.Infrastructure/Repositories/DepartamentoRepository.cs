﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class DepartamentoRepository : IDepartamentoRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public DepartamentoRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }
        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_TD_DEPA_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<DepartamentoVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<DepartamentoVM>>(_context.TS_TD_DEPA_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public DepartamentoVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(DepartamentoVM model)
        {
            try
            {
                int? result = _context.TS_TD_DEPA_I01(model.IdCondominio
                    , model.NombreDepartamento
                    , model.Direccion
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(DepartamentoVM model)
        {
            try
            {
                int? result = _context.TS_TD_DEPA_U01(model.IdDepartamento
                    , model.IdCondominio
                    , model.NombreDepartamento
                    , model.Direccion
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
