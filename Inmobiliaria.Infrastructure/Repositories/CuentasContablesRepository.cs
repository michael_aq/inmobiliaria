﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class CuentasContablesRepository : ICuentasContablesRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public CuentasContablesRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }

        public int Delete(String model)
        {
            try
            {
                var result = _context.TS_COM_CuentasContables_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<CuentasContablesVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<CuentasContablesVM>>(_context.TS_COM_CuentasContables_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public CuentasContablesVM GetByID(String Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(CuentasContablesVM model)
        {
            try
            {
                int? result = _context.TS_COM_CuentasContables_I01(
                      model.CTA_CtaCon
                    , model.CTA_Des
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(CuentasContablesVM model)
        {
            try
            {
                int? result = _context.TS_COM_CuentasContables_U01(
                    model.CTA_CtaCon
                    , model.CTA_Des
                    , model.CTA_EstRegA
                    , _user);
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
