﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class ArchivoRepository : IArchivoRepository
    {
        private readonly INMOBILIARIAEntities _context;
        private readonly IMapper _mapper;
        private readonly string _user;
        public ArchivoRepository(INMOBILIARIAEntities context)
        {
            _context = context;
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
            _user = HttpContext.Current.User.Identity.Name;
        }
        public int Delete(int model)
        {
            try
            {
                var result = _context.TS_TD_DOCU_DEPA_D01(model);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ICollection<ArchivoVM> GetAll()
        {
            try
            {
                var result = _mapper.Map<List<ArchivoVM>>(_context.TS_TD_DOCU_DEPA_Q01());
                return result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public ArchivoVM GetByID(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(ArchivoVM model)
        {
            try
            {
                int? result = _context.TS_TD_DOCU_DEPA_I01(model.IdGuia
                    , model.Nombre
                    , model.Codigo
                    , _user).FirstOrDefault();
                return result.Value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int update(ArchivoVM model)
        {
            throw new NotImplementedException();
        }
    }
}
