﻿using AutoMapper;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.Common.Mappers.Init;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Inmobiliaria.Core.Common;

namespace Inmobiliaria.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMapper _mapper;

        public UserRepository()
        {
            _mapper = InfrastructureMapperBoostrap.MapperConfiguration.CreateMapper();
        }
        public ICollection<UserResultVM> GetUserByUserName(string userName)
        {
            var context = new INMOBILIARIAEntities();
            return _mapper.Map<IList<UserResultVM>>(context.TS_TM_TRAB_Q01(userName).ToList());
        }
        public IList<string> GetRolesByUser(string userCode)
        {
            List<string> roles = new List<string>();
            using (var context = new INMOBILIARIAEntities())
            {
                roles = context.TS_TM_ACCE_Q01(userCode).ToList();
            }
            return roles;
 
        }
        public bool ValidateUserPassword(string userName, string encodePass)
        {
            bool rtpa = false;
 
            TM_TRAB user = null;
            using (var context = new INMOBILIARIAEntities())
            {
                if (context.Set<TM_TRAB>().Any(m => m.CO_USER_NAME == userName))
                {
                    user = context.Set<TM_TRAB>().FirstOrDefault(m => m.CO_USER_NAME == userName);
                    rtpa = String.Compare(encodePass, user.CL_USUA, StringComparison.InvariantCultureIgnoreCase) == 0;
                }
            }
            return rtpa;
            
        }
        public int UpdateWorker(string pass)
        {
            throw new NotImplementedException();
        }

        //public ICollection<UserResultVM> GetAllUsers()
        //{
        //    try
        //    {
        //        var resList = new List<UserResultVM>();
        //        using (var context = new INMOBILIARIAEntities())
        //        {
        //            resList = _mapper.Map<List<UserResultVM>>(context.TS_TM_TRAB_Q02());

        //        }
        //        return resList;
        //    }catch(Exception e)
        //    {
        //        throw e;
        //    }
        //}

        //public int? InsertUser(UserResultVM model, string user)
        //{
        //    try
        //    {
        //        using (var context = new INMOBILIARIAEntities())
        //        {
        //            if (!model.EsUsuarioAD)
        //            {
        //                var passEncoded = Util.EncodePassword(model.Contrasenia.Trim());
        //                var res = context.TS_TM_TRAB_I01(model.NombreTrabajador.Trim(), model.CodUserName.Trim(), passEncoded,
        //                model.EsUsuarioAD, model.Rol, user);
        //                return res.FirstOrDefault();
        //            }
        //            else
        //            {
        //                model.Contrasenia = "";
        //                var res = context.TS_TM_TRAB_I01(model.NombreTrabajador.Trim(), model.CodUserName.Trim(), model.Contrasenia.Trim(),
        //                model.EsUsuarioAD, model.Rol, user);
        //                return res.FirstOrDefault();
        //            }
        //        }
        //    }catch(Exception e)
        //    {
        //        throw e;
        //    }
        //}

        //public  int? UpdateUser(UserResultVM model, string user)
        //{
        //    try
        //    {
        //        using (var context = new INMOBILIARIAEntities())
        //        {
        //            if (!model.EsUsuarioAD)
        //            {
        //                if (String.IsNullOrEmpty(model.Contrasenia.Trim()))
        //                {
        //                    var res = context.TS_TM_TRAB_U02(model.CodigoTrabajador, model.NombreTrabajador.Trim(),
        //                       true, model.CodUserName.Trim(),
        //                       model.EsUsuarioAD, model.Rol, user);
        //                    return res.FirstOrDefault();
        //                }
        //                else
        //                {
        //                    var passEncoded = Util.EncodePassword(model.Contrasenia.Trim());
        //                    var res = context.TS_TM_TRAB_U01(model.CodigoTrabajador, model.NombreTrabajador.Trim(), 
        //                        true, model.CodUserName.Trim(),
        //                        passEncoded, model.EsUsuarioAD, model.Rol, user);
        //                    return res.FirstOrDefault();
        //                }
        //            }
        //            else {
        //                model.Contrasenia = "";
        //                var res = context.TS_TM_TRAB_U01(model.CodigoTrabajador, model.NombreTrabajador.Trim(), true, model.CodUserName.Trim(),
        //                model.Contrasenia.Trim(), model.EsUsuarioAD, model.Rol, user);
        //                return res.FirstOrDefault();
        //            }
                    
        //        }
        //    }catch(Exception e)
        //    {
        //        throw e;
        //    }
        //}


    }
}
