//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inmobiliaria.Infrastructure.DBModel
{
    using System;
    
    public partial class TS_COM_CuentasContables_Q02_Result
    {
        public string CTA_CtaCon { get; set; }
        public string CTA_Des { get; set; }
        public string CTA_EstRegA { get; set; }
        public string US_CREA { get; set; }
        public System.DateTime FE_CREA { get; set; }
        public string US_MODI { get; set; }
        public System.DateTime FE_MODI { get; set; }
    }
}
