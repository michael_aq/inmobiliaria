//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inmobiliaria.Infrastructure.DBModel
{
    using System;
    
    public partial class TS_P0M_Proveedor_Q02_Result
    {
        public int PRO_Cod { get; set; }
        public string PRO_Nom { get; set; }
        public string PRO_Ruc { get; set; }
        public string TIPCONFIS_TipConFis { get; set; }
        public System.DateTime PRO_FecIns { get; set; }
        public string PRO_CtaCte { get; set; }
        public string PRO_Dir { get; set; }
        public string PRO_DomFis { get; set; }
        public string PRO_EstReg { get; set; }
        public string TIPCONFIS_EstReg { get; set; }
        public string US_CREA { get; set; }
        public System.DateTime FE_CREA { get; set; }
        public string US_MODI { get; set; }
        public System.DateTime FE_MODI { get; set; }
    }
}
