//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inmobiliaria.Infrastructure.DBModel
{
    using System;
    
    public partial class TS_TR_RESE_Q01_Result
    {
        public int ID_RESE { get; set; }
        public int ID_DEPA { get; set; }
        public string NB_DEPA { get; set; }
        public int ID_VEND { get; set; }
        public string NB_VEND { get; set; }
        public int ID_CLIE { get; set; }
        public string NB_CLIE { get; set; }
        public decimal RESE_VALOR { get; set; }
        public Nullable<System.DateTime> FE_LIMI { get; set; }
        public string ES_ACTI { get; set; }
        public string ESTA_DETA { get; set; }
    }
}
