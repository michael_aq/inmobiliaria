﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class TipoOrdenMapperQ01 : Profile
    {
        public TipoOrdenMapperQ01()
        {
            CreateMap<TS_GZZ_Tipo_Orden_Q01_Result, TipoOrdenVM>()

            .ForMember(tar => tar.TIPORD_TipOrd, mapper => mapper.MapFrom(sour => sour.TIPORD_TipOrd))
            .ForMember(tar => tar.TIPORD_Des, mapper => mapper.MapFrom(sour => sour.TIPORD_Des))
               .ReverseMap();
        }
    }
}
