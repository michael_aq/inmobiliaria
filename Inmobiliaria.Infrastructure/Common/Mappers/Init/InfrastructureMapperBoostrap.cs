﻿using AutoMapper;

namespace Inmobiliaria.Infrastructure.Common.Mappers.Init
{
    public class InfrastructureMapperBoostrap
    {
        public static MapperConfiguration MapperConfiguration;

        public static void Configure()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UsuarioMapperQ01());
                cfg.AddProfile(new ClienteMapperQ01());
                //cfg.AddProfile(new CondominioMapperQ01());
                cfg.AddProfile(new DepartamentoMapperQ01());
                cfg.AddProfile(new VendedorMapperQ01());
                cfg.AddProfile(new ReservaMapperQ01());
                cfg.AddProfile(new CuentasContablesMapperQ01());
                cfg.AddProfile(new CuentasContablesMapperQ02());
                cfg.AddProfile(new FondoFijoMapperQ01());
                cfg.AddProfile(new ReembolsoMapperQ01());
                cfg.AddProfile(new RendicionCABMapperQ01());
                cfg.AddProfile(new RendicionDETMapperQ01());
                cfg.AddProfile(new ProveedorMapperQ01());
                // TABLAS GENERALES Y REFERENCIALES
                cfg.AddProfile(new EstadoRegistroMapperQ01());
                cfg.AddProfile(new DivisionMapperQ01());
                cfg.AddProfile(new TipoCondicionFiscalMapperQ01());
                cfg.AddProfile(new TipoImpuestoMapperQ01());
                cfg.AddProfile(new TipoOrdenMapperQ01());
            });
        }

    }
}
