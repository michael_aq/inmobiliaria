﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class EstadoRegistroMapperQ01 : Profile
    {
        public EstadoRegistroMapperQ01()
        {
            CreateMap<TS_GZZ_Estado_Registro_Q01_Result, EstadoRegistroVM>()
        .ForMember(tar => tar.ESTREG_EstReg, mapper => mapper.MapFrom(sour => sour.ES_ACTI))
        .ForMember(tar => tar.ESTREG_Des, mapper => mapper.MapFrom(sour => sour.ESTA_DETA))
         .ReverseMap();
        }
    }
}
