﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class TipoCondicionFiscalMapperQ01 : Profile
    {
        public TipoCondicionFiscalMapperQ01()
        {
            CreateMap<TS_GZZ_Tipo_Condicion_Fiscal_Q01_Result, TipoCondicionFiscalVM>()
            .ForMember(tar => tar.TIPCONFIS_TipConFis, mapper => mapper.MapFrom(sour => sour.TIPCONFIS_TipConFis))
            .ForMember(tar => tar.TIPCONFIS_Des, mapper => mapper.MapFrom(sour => sour.TIPCONFIS_Des))
              .ReverseMap();
        }
    }
}
