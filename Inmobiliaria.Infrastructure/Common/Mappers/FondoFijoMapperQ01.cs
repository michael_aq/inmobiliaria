﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class FondoFijoMapperQ01 : Profile
    {
        public FondoFijoMapperQ01()
        {
            CreateMap<TS_F1M_Fondo_fijo_Q01_Result, FondoFijoVM>()
                .ForMember(tar => tar.FF_Cod, mapper => mapper.MapFrom(sour => sour.FF_Cod))
                .ForMember(tar => tar.FF_CtaCte, mapper => mapper.MapFrom(sour => sour.FF_CtaCte))
                .ForMember(tar => tar.FF_CtaCon, mapper => mapper.MapFrom(sour => sour.FF_CtaCon))
                .ForMember(tar => tar.FF_imp, mapper => mapper.MapFrom(sour => sour.FF_imp))
                .ForMember(tar => tar.FF_Div, mapper => mapper.MapFrom(sour => sour.FF_Div))
                .ForMember(tar => tar.FF_EstReg, mapper => mapper.MapFrom(sour => sour.FF_EstReg))
                .ForMember(tar => tar.FF_EstRegCtaCon, mapper => mapper.MapFrom(sour => sour.FF_EstRegCtaCon))
                .ForMember(tar => tar.FF_Des, mapper => mapper.MapFrom(sour => sour.FF_Des))
               .ReverseMap();
        }
    }
}
