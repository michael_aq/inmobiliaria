﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class CuentasContablesMapperQ01 : Profile
    {
        public CuentasContablesMapperQ01()
        {
            CreateMap<TS_COM_CuentasContables_Q01_Result, CuentasContablesVM>()

               .ForMember(tar => tar.CTA_CtaCon, mapper => mapper.MapFrom(sour => sour.CTA_CtaCon))
               .ForMember(tar => tar.CTA_Des, mapper => mapper.MapFrom(sour => sour.CTA_Des))
               .ForMember(tar => tar.CTA_EstRegA, mapper => mapper.MapFrom(sour => sour.CTA_EstRegA))
               .ReverseMap();
        }
    }
}
