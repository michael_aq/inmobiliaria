﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class UsuarioMapperQ01 : Profile
    {
        public UsuarioMapperQ01()
        {
            CreateMap<TS_TM_TRAB_Q01_Result, UserResultVM>()

               .ForMember(tar => tar.CodigoTrabajador, mapper => mapper.MapFrom(sour => sour.ID_TRAB))
               .ForMember(tar => tar.NombreTrabajador, mapper => mapper.MapFrom(sour => sour.NB_TRAB))
               .ForMember(tar => tar.CodUserName, mapper => mapper.MapFrom(sour => sour.CO_USER_NAME))
               .ForMember(tar => tar.EsUsuarioAD, mapper => mapper.MapFrom(sour => sour.ES_AUTH_AD))

               .ReverseMap();
        }
    }
}
