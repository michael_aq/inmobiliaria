﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class CondominioMapperQ01 : Profile
    {
        public CondominioMapperQ01()
        {
            CreateMap<TS_TM_COND_Q01_Result, CondominioVM>()

               .ForMember(tar => tar.IdCondominio, mapper => mapper.MapFrom(sour => sour.ID_COND))
               .ForMember(tar => tar.CondominioDireccion, mapper => mapper.MapFrom(sour => sour.COND_DIRE))
               .ForMember(tar => tar.NombreCondominio, mapper => mapper.MapFrom(sour => sour.NB_COND))
               .ForMember(tar => tar.CondominioRuta, mapper => mapper.MapFrom(sour => sour.COND_RUTA))
               .ForMember(tar => tar.Ubicacion, mapper => mapper.MapFrom(sour => sour.COND_UBI))
               .ForMember(tar => tar.Estado, mapper => mapper.MapFrom(sour => sour.ES_ACTI))
               .ForMember(tar => tar.DescripcionEstado, mapper => mapper.MapFrom(sour => sour.ESTA_DETA))

               .ReverseMap();
        }
    }
}
