﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;


namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class RendicionCABMapperQ01 : Profile
    {
        public RendicionCABMapperQ01()
        {
            CreateMap<TS_F3T_Rendicion_CAB_Q01_Result, RendicionCABVM>()
                .ForMember(tar => tar.IdRendicion, mapper => mapper.MapFrom(sour => sour.RENC_Cod))
                .ForMember(tar => tar.FechaCreacionRendicion, mapper => mapper.MapFrom(sour => sour.RENC_FecCre))
                .ForMember(tar => tar.FechaEnvioRendicion, mapper => mapper.MapFrom(sour => sour.RENC_FecEnv))
                .ForMember(tar => tar.FF_Cod, mapper => mapper.MapFrom(sour => sour.FF_Cod))
                .ForMember(tar => tar.FF_CtaCon, mapper => mapper.MapFrom(sour => sour.FF_CtaCon))
                .ForMember(tar => tar.FF_Div, mapper => mapper.MapFrom(sour => sour.FF_Div))
                .ForMember(tar => tar.EstadoRegistroRendicion, mapper => mapper.MapFrom(sour => sour.RENC_EstReg))
                .ForMember(tar => tar.TotalRendicion, mapper => mapper.MapFrom(sour => sour.RENC_Total))

                .ForMember(tar => tar.RENC_ENVI_REND, mapper => mapper.MapFrom(sour => sour.RENC_ENVI_REND))
                          .ReverseMap();
        }
    }
}
