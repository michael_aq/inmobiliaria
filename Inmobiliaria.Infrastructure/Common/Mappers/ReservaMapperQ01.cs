﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class ReservaMapperQ01 : Profile
    {
        public ReservaMapperQ01()
        {
            CreateMap<TS_TR_RESE_Q01_Result, ReservaVM>()

               .ForMember(tar => tar.IdReserva, mapper => mapper.MapFrom(sour => sour.ID_RESE))
               .ForMember(tar => tar.IdDepartamento, mapper => mapper.MapFrom(sour => sour.ID_DEPA))
               .ForMember(tar => tar.NombreDepartamento, mapper => mapper.MapFrom(sour => sour.NB_DEPA))
               .ForMember(tar => tar.IdVendedor, mapper => mapper.MapFrom(sour => sour.ID_VEND))
               .ForMember(tar => tar.NombreVendedor, mapper => mapper.MapFrom(sour => sour.NB_VEND))
               .ForMember(tar => tar.IdCliente, mapper => mapper.MapFrom(sour => sour.ID_CLIE))
               .ForMember(tar => tar.NombreCliente, mapper => mapper.MapFrom(sour => sour.NB_CLIE))
               .ForMember(tar => tar.ValorReserva, mapper => mapper.MapFrom(sour => sour.RESE_VALOR))
               .ForMember(tar => tar.FechaLimite, mapper => mapper.MapFrom(sour => sour.FE_LIMI))
               .ForMember(tar => tar.Estado, mapper => mapper.MapFrom(sour => sour.ES_ACTI))
               .ForMember(tar => tar.DescripcionEstado, mapper => mapper.MapFrom(sour => sour.ESTA_DETA))

               .ReverseMap();
        }
    }
}
