﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class DivisionMapperQ01 : Profile
    {
        public DivisionMapperQ01()
        {
            CreateMap<TS_GZM_Division_Q01_Result, DivisionVM>()

               .ForMember(tar => tar.DIV_Codigo, mapper => mapper.MapFrom(sour => sour.DIV_Cod))
               .ForMember(tar => tar.DIV_Descripcion, mapper => mapper.MapFrom(sour => sour.DIV_Des))
               .ForMember(tar => tar.DIV_Direccion, mapper => mapper.MapFrom(sour => sour.DIV_Direc))
               .ForMember(tar => tar.DIV_EstRegistro, mapper => mapper.MapFrom(sour => sour.DIV_EstReg))
               .ForMember(tar => tar.DIV_Ruta, mapper => mapper.MapFrom(sour => sour.DIV_Ruta))
               .ForMember(tar => tar.DIV_Ubigeo, mapper => mapper.MapFrom(sour => sour.DIV_Ubi))
               .ReverseMap();
        }
    }
}
