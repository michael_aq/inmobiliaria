﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class ArchivoMapperQ01 : Profile
    {
        public ArchivoMapperQ01()
        {
            CreateMap<TS_TD_DOCU_DEPA_Q01_Result, ArchivoVM>()

               .ForMember(tar => tar.IdArchivo, mapper => mapper.MapFrom(sour => sour.ID_DOC_DEPA))
               .ForMember(tar => tar.IdGuia, mapper => mapper.MapFrom(sour => sour.ID_DEPA))
               .ForMember(tar => tar.Codigo, mapper => mapper.MapFrom(sour => sour.COD_DOCU_DEPA))
               .ForMember(tar => tar.Nombre, mapper => mapper.MapFrom(sour => sour.NB_DOCU_DEPA))
               .ForMember(tar => tar.Estado, mapper => mapper.MapFrom(sour => sour.ES_ACTI))

               .ReverseMap();
        }
    }
}
