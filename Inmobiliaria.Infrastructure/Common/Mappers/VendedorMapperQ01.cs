﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class VendedorMapperQ01 : Profile
    {
        public VendedorMapperQ01()
        {
            CreateMap<TS_TM_VEND_Q01_Result, VendedorVM>()

               .ForMember(tar => tar.IdVendedor, mapper => mapper.MapFrom(sour => sour.ID_VEND))
               .ForMember(tar => tar.NombreVendedor, mapper => mapper.MapFrom(sour => sour.NB_VEND))
               .ForMember(tar => tar.ApellidoPaterno, mapper => mapper.MapFrom(sour => sour.VEND_APEL_PATE))
               .ForMember(tar => tar.ApellidoMaterno, mapper => mapper.MapFrom(sour => sour.VEND_APEL_MATE))
               .ForMember(tar => tar.Direccion, mapper => mapper.MapFrom(sour => sour.VEND_DIRE))
               .ForMember(tar => tar.DNI, mapper => mapper.MapFrom(sour => sour.VEND_DNI))
               .ForMember(tar => tar.Telefono, mapper => mapper.MapFrom(sour => sour.VEND_TELE))
               .ForMember(tar => tar.CorreoElectronico, mapper => mapper.MapFrom(sour => sour.VEND_CORR)) 
               .ForMember(tar => tar.Estado, mapper => mapper.MapFrom(sour => sour.ES_ACTI))
               .ForMember(tar => tar.DescripcionEstado, mapper => mapper.MapFrom(sour => sour.ESTA_DETA))

               .ReverseMap();
        }
    }
}
