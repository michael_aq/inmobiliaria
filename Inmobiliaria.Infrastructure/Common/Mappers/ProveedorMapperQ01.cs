﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;


namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class ProveedorMapperQ01 : Profile
    {
        public ProveedorMapperQ01()
        {
            CreateMap<TS_P0M_Proveedor_Q01_Result, ProveedorVM>()
                .ForMember(tar => tar.PRO_Cod, mapper => mapper.MapFrom(sour => sour.PRO_Cod))
                .ForMember(tar => tar.PRO_Nom, mapper => mapper.MapFrom(sour => sour.PRO_Nom))
                .ForMember(tar => tar.PRO_Ruc, mapper => mapper.MapFrom(sour => sour.PRO_Ruc))
                .ForMember(tar => tar.TIPCONFIS_TipConFis, mapper => mapper.MapFrom(sour => sour.TIPCONFIS_TipConFis))
                .ForMember(tar => tar.PRO_FecIns, mapper => mapper.MapFrom(sour => sour.PRO_FecIns))
                .ForMember(tar => tar.PRO_CtaCte, mapper => mapper.MapFrom(sour => sour.PRO_CtaCte))
                .ForMember(tar => tar.PRO_Dir, mapper => mapper.MapFrom(sour => sour.PRO_Dir))
                .ForMember(tar => tar.PRO_DomFis, mapper => mapper.MapFrom(sour => sour.PRO_DomFis))
                .ForMember(tar => tar.PRO_EstReg, mapper => mapper.MapFrom(sour => sour.PRO_EstReg))
                          .ReverseMap();
        }
    }
}
