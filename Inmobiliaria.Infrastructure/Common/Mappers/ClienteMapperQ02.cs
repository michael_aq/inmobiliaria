﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class ClienteMapperQ02 : Profile
    {
        public ClienteMapperQ02()
        {
            CreateMap<TS_TM_CLIE_Q02_Result,  ClienteVM>()
                
               .ForMember(tar => tar.IdCliente, mapper => mapper.MapFrom(sour => sour.ID_CLIE))
               .ForMember(tar => tar.NombreCliente, mapper => mapper.MapFrom(sour => sour.NB_CLIE))
               .ForMember(tar => tar.ApellidoPaterno, mapper => mapper.MapFrom(sour => sour.CLIE_APEL_PATE))
               .ForMember(tar => tar.ApellidoMaterno, mapper => mapper.MapFrom(sour => sour.CLIE_APEL_MATE))
               .ForMember(tar => tar.Direccion, mapper => mapper.MapFrom(sour => sour.CLIE_DIRE))
               .ForMember(tar => tar.DNI, mapper => mapper.MapFrom(sour => sour.CLIE_DNI))
               .ForMember(tar => tar.Telefono, mapper => mapper.MapFrom(sour => sour.CLIE_TEL))
               .ForMember(tar => tar. CorreoElectronico, mapper => mapper.MapFrom(sour => sour.CLIE_COR))
               .ForMember(tar => tar.Estado, mapper => mapper.MapFrom(sour => sour.ES_ACTI))
               .ForMember(tar => tar.UsuarioCrea, mapper => mapper.MapFrom(sour => sour.US_CREA))
               .ForMember(tar => tar.FechaCrea, mapper => mapper.MapFrom(sour => sour.FE_CREA))
               .ForMember(tar => tar.UsuarioModi, mapper => mapper.MapFrom(sour => sour.US_MODI))
               .ForMember(tar => tar.FechaModi, mapper => mapper.MapFrom(sour => sour.FE_MODI))

               .ReverseMap();
        }
    }
}
