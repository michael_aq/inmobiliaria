﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;


namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class ReembolsoMapperQ01 : Profile
    {
        public ReembolsoMapperQ01()
        {
            CreateMap<TS_F2T_Rembolso_Q01_Result, ReembolsoVM>()
                .ForMember(tar => tar.REM_Cod, mapper => mapper.MapFrom(sour => sour.REM_Cod))
                .ForMember(tar => tar.REM_Imp, mapper => mapper.MapFrom(sour => sour.REM_Imp))
                .ForMember(tar => tar.REM_CtaCte, mapper => mapper.MapFrom(sour => sour.REM_CtaCte))
                .ForMember(tar => tar.REM_FecEnv, mapper => mapper.MapFrom(sour => sour.REM_FecEnv))
                .ForMember(tar => tar.REM_FecCon, mapper => mapper.MapFrom(sour => sour.REM_FecCon))
                .ForMember(tar => tar.REM_EstReg, mapper => mapper.MapFrom(sour => sour.REM_EstReg))
                .ForMember(tar => tar.IdRendicion, mapper => mapper.MapFrom(sour => sour.RENC_Cod))
                //.ForMember(tar => tar.FF_CtaCon, mapper => mapper.MapFrom(sour => sour.FF_CtaCon))
                //.ForMember(tar => tar.FF_Div, mapper => mapper.MapFrom(sour => sour.FF_Div))
                //.ForMember(tar => tar.FF_EstReg, mapper => mapper.MapFrom(sour => sour.FF_EstReg))
                //.ForMember(tar => tar.FF_EstRegCtaCon, mapper => mapper.MapFrom(sour => sour.FF_EstRegCtaCon))
                //.ForMember(tar => tar.FF_Cod, mapper => mapper.MapFrom(sour => sour.FF_Cod))
                .ForMember(tar => tar.RENC_EstReg, mapper => mapper.MapFrom(sour => sour.RENC_EstReg))
                .ForMember(tar => tar.REM_APRO, mapper => mapper.MapFrom(sour => sour.REM_APRO))
                          .ReverseMap();
        }
    }
}
