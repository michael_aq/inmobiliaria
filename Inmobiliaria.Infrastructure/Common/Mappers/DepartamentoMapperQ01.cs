﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class DepartamentoMapperQ01 : Profile
    {
        public DepartamentoMapperQ01()
        {
            CreateMap<TS_TD_DEPA_Q01_Result, DepartamentoVM>()

               .ForMember(tar => tar.IdDepartamento, mapper => mapper.MapFrom(sour => sour.ID_DEPA))
               .ForMember(tar => tar.IdCondominio, mapper => mapper.MapFrom(sour => sour.ID_COND))
               .ForMember(tar => tar.NombreCondominio, mapper => mapper.MapFrom(sour => sour.DIV_Des))
               .ForMember(tar => tar.NombreDepartamento, mapper => mapper.MapFrom(sour => sour.NB_DEPA))
               .ForMember(tar => tar.Direccion, mapper => mapper.MapFrom(sour => sour.DEPA_DIRE))
               .ForMember(tar => tar.DepartamentoCodigo, mapper => mapper.MapFrom(sour => sour.DEPA_CODI))
               .ForMember(tar => tar.Estado, mapper => mapper.MapFrom(sour => sour.ES_ACTI))
               .ForMember(tar => tar.DescripcionEstado, mapper => mapper.MapFrom(sour => sour.ESTA_DETA))

               .ReverseMap();
        }
    }
}
