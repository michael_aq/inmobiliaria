﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class CuentasContablesMapperQ02 : Profile
    {
        public CuentasContablesMapperQ02()
        {
            CreateMap<TS_COM_CuentasContables_Q02_Result, CuentasContablesVM>()

               .ForMember(tar => tar.CTA_CtaCon, mapper => mapper.MapFrom(sour => sour.CTA_CtaCon))
               .ForMember(tar => tar.CTA_Des, mapper => mapper.MapFrom(sour => sour.CTA_Des))
               .ForMember(tar => tar.CTA_EstRegA, mapper => mapper.MapFrom(sour => sour.CTA_EstRegA))
               .ForMember(tar => tar.UsuarioCrea, mapper => mapper.MapFrom(sour => sour.US_CREA))
               .ForMember(tar => tar.FechaCrea, mapper => mapper.MapFrom(sour => sour.FE_CREA))
               .ForMember(tar => tar.UsuarioModi, mapper => mapper.MapFrom(sour => sour.US_MODI))
               .ForMember(tar => tar.FechaModi, mapper => mapper.MapFrom(sour => sour.FE_MODI))
               .ReverseMap();
        }
    }
}
