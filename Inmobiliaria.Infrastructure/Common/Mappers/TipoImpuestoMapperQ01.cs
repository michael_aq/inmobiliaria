﻿using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class TipoImpuestoMapperQ01 : Profile
    {
        public TipoImpuestoMapperQ01()
        {
            CreateMap<TS_GZZ_Tipo_Impuesto_Q01_Result, TipoImpuestoVM>()
            .ForMember(tar => tar.TIPIMP_TipImp, mapper => mapper.MapFrom(sour => sour.TIPIMP_TipImp))
            .ForMember(tar => tar.TIPIMP_Des, mapper => mapper.MapFrom(sour => sour.TIPIMP_Des))
            .ForMember(tar => tar.TIPIMP_Por, mapper => mapper.MapFrom(sour => sour.TIPIMP_Por))
             .ReverseMap();
        }
    }
}
