﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmobiliaria.Core.ViewModels;
using Inmobiliaria.Infrastructure.DBModel;


namespace Inmobiliaria.Infrastructure.Common.Mappers
{
    class RendicionDETMapperQ01 : Profile
    {
        public RendicionDETMapperQ01()
        {
            CreateMap<TS_F3T_Rendicion_DET_Q01_Result, RendicionDETVM>()
                .ForMember(tar => tar.REND_Cod, mapper => mapper.MapFrom(sour => sour.REND_Cod))
                .ForMember(tar => tar.DIV_Cod, mapper => mapper.MapFrom(sour => sour.DIV_Cod))
                .ForMember(tar => tar.REND_FecCon, mapper => mapper.MapFrom(sour => sour.REND_FecCon))
                .ForMember(tar => tar.REND_FecDoc, mapper => mapper.MapFrom(sour => sour.REND_FecDoc))
                .ForMember(tar => tar.REND_TipDoc, mapper => mapper.MapFrom(sour => sour.REND_TipDoc))
                .ForMember(tar => tar.REND_DocRef, mapper => mapper.MapFrom(sour => sour.REND_DocRef))
                .ForMember(tar => tar.TIPIMP_TipImp, mapper => mapper.MapFrom(sour => sour.TIPIMP_TipImp))
                .ForMember(tar => tar.REND_MonGas, mapper => mapper.MapFrom(sour => sour.REND_MonGas))
                .ForMember(tar => tar.REND_Des, mapper => mapper.MapFrom(sour => sour.REND_Des))
                .ForMember(tar => tar.TIPORD_TipOrd, mapper => mapper.MapFrom(sour => sour.TIPORD_TipOrd))
                .ForMember(tar => tar.REND_CtaCon, mapper => mapper.MapFrom(sour => sour.REND_CtaCon))
                .ForMember(tar => tar.REND_EstRegCtaCon, mapper => mapper.MapFrom(sour => sour.REND_EstRegCtaCon))
                //.ForMember(tar => tar.TIPORD_EstReg, mapper => mapper.MapFrom(sour => sour.TIPORD_EstReg))
                .ForMember(tar => tar.IdRendicion, mapper => mapper.MapFrom(sour => sour.RENC_Cod))
                .ForMember(tar => tar.FF_CtaCon, mapper => mapper.MapFrom(sour => sour.FF_CtaCon))
                .ForMember(tar => tar.FF_Div, mapper => mapper.MapFrom(sour => sour.FF_Div))
                //.ForMember(tar => tar.FF_EstReg, mapper => mapper.MapFrom(sour => sour.FF_EstReg))
                //.ForMember(tar => tar.FF_EstRegCtaCon, mapper => mapper.MapFrom(sour => sour.FF_EstRegCtaCon))
                .ForMember(tar => tar.FF_Cod, mapper => mapper.MapFrom(sour => sour.FF_Cod))
                .ForMember(tar => tar.PRO_Cod, mapper => mapper.MapFrom(sour => sour.PRO_Cod))

                .ForMember(tar => tar.REND_OBSE_MODI, mapper => mapper.MapFrom(sour => sour.REND_OBSE_MODI))
                          .ReverseMap();
        }
    }
}
