﻿using System;
using System.Security.Claims;


namespace SistVigilancia_Caja.Models.Auth
{
    public class UserVM : ClaimsPrincipal
    {
        public UserVM(ClaimsPrincipal principal)
        : base(principal)
        {
        }
        public int WorkerCode
        {
            get
            {
                var v = FindFirst("WorkerCode");
                int code = 0;
                if (v != null)
                {

                    Int32.TryParse(v.Value, out code);
                }
                return code;
            }
        } //co_tra
        public string WorkerCodeExt
        {
            get
            {
                var v = FindFirst("WorkerCodeExt");
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }

        } //co_exte

        public int SubdivisionCode
        {
            get
            {
                var v = FindFirst("SubdivisionCode");
                int code = 0;
                if (v != null)
                {

                    Int32.TryParse(v.Value, out code);
                }
                return code;
            }
        }
        public string SubdivisionDescri
        {
            get
            {
                var v = FindFirst("SubdivisionDescri");
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }
        }
        public int BossCode
        {

            get
            {
                var v = FindFirst("BossCode");
                int code = 0;
                if (v != null)
                {

                    Int32.TryParse(v.Value, out code);
                }
                return code;
            }

        }
        public string BossName
        {
            get
            {
                var v = FindFirst("BossName");
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }
        }
        public int ManagerCode
        {

            get
            {
                var v = FindFirst("ManagerCode");
                int code = 0;
                if (v != null)
                {

                    Int32.TryParse(v.Value, out code);
                }
                return code;
            }

        }
        public string ManagerName
        {
            get
            {
                var v = FindFirst("ManagerName");
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }


        }

        public string DocumentNumber
        {

            get
            {
                var v = FindFirst("DocumentNumber");
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }
        }
        public string UserName
        {

            get
            {
                var v = FindFirst(ClaimTypes.Name);
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }
        }
        public string Name
        {
            get
            {
                var v = FindFirst(ClaimTypes.GivenName);
                string value = "";
                if (v != null)
                {
                    value = v.Value;
                }
                return value;
            }
        }

    }
}