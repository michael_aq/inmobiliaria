﻿using System;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SistInmobiliaria.Startup))]
namespace SistInmobiliaria
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
       
    }
}
