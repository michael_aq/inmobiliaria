﻿var gridManager = function () {

    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    var idDepa = 0;
    return {
        // GridEvents
        onEdit: function (e) {
            
            if (e.model.isNew()) {
                $('.k-window-title').text("Registrar Nueva Reserva").width(700);
                idDepa = 0;                
                $("#IdDepartamento").data("kendoDropDownList").dataSource.read();
            } 
            else {
                $('.k-window-title').text("Editar Reserva").width(700);
                idDepa = e.model.IdDepartamento;
                $("#IdDepartamento").data("kendoDropDownList").dataSource.read();
                $("#IdDepartamento").data("kendoDropDownList").value(idDepa);
            }
        },
        filtro: function () {
            return {tipo:1,id:idDepa,}
        },
        onSave: function (e) {
            if (!ReservaValidator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function ReservaValidator() {
    var Validator = $("#ReservaContainer").kendoValidator({
        rules: {
            ReservaObligatoria: function (input) {
                var result = true
                var yy = $("#IdDepartamento").data("kendoDropDownList").value();
                if (yy == 0) {
                    //if (input.val().trim() == "")
                    result = false;
                }
                var yy = $("#IdVendedor").data("kendoDropDownList").value();
                if (yy == 0) {
                    //if (input.val().trim() == "")
                    result = false;
                }
                var yy = $("#IdCliente").data("kendoDropDownList").value();
                if (yy == 0) {
                    //if (input.val().trim() == "")
                    result = false;
                }
                var yy = $("#ValorReserva").data("kendoNumericTextBox").value();
                if (yy == "") {
                    //if (input.val().trim() == "")
                    result = false;
                }
                return result;
            }
        },
        messages: {
            ReservaObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}