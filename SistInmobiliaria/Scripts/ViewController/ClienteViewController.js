﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {
                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            if(e.model.isNew()) {
                $('.k-window-title').text("Registrar Nueva Cliente").width(700);
                $("#EstadoDrop").hide();
            } else {
                $('.k-window-title').text("Editar Cliente").width(700);
                $("#EstadoDrop").show();
            }
        },
        onSave: function (e) {
            if (!ClienteValidator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function ClienteValidator() {
    var Validator = $("#ClienteContainer").kendoValidator({
        rules: {
            ClienteObligatoria: function (input) {
                var result = true;
                if (input.is("[name=NombreCliente]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=ApellidoPaterno]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=ApellidoMaterno]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=Direccion]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                var expreg = new RegExp("^[0-9]{8}$");
                if (input.is("[name=DNI]")) {
                    if (input.val().trim() == "" )
                        result = false;
                    else if (!expreg.test(input.val()))
                        result = false;

                }
                expreg = new RegExp("^[9]{1}[0-9]{8}$");
                if (input.is("[name=Telefono]")) {
                    if (input.val().trim() == "" )
                        result = false;
                    else if (!expreg.test(input.val()))
                        result = false;
                }
                expreg = new RegExp("^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$");
                if (input.is("[name=CorreoElectronico]")) {
                    if (input.val().trim() == "")
                        result = false;
                    else if (!expreg.test(input.val()))
                        result = false;
                }
                return result;
            }
        },
        messages: {
            ClienteObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}