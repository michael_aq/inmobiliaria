﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nuevo FondoFijo").width(700) : $('.k-window-title').text("Editar FondoFijo").width(700);
            if (e.model.isNew()) $("#EstadoRegistro").hide()
            else $("#EstadoRegistro").show()
        },
        onSave: function (e) {
            if (!FondoFijoValidator()) {
                e.preventDefault();
            }
            e.model.FF_CtaCon = $("#FF_CtaCon").data("kendoDropDownList").value()
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function FondoFijoValidator() {
    var Validator = $("#FondoFijoContainer").kendoValidator({
        rules: {
            FondoObligatoria: function (input) {
                var result = true;
                //if ($("#FF_CtaCon").val()==0) {
                //       result = false;
                //}
                //$("#FF_CtaCon").val($("#FF_CtaCon").val().trim());
                return result;
            }
        },
        messages: {
            FondoObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}