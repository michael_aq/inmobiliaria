﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nueva Division").width(700) : $('.k-window-title').text("Editar Division").width(700);
        },
        onSave: function (e) {
            if (!CondominioValidator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function CondominioValidator() {
    var Validator = $("#DivisionContainer").kendoValidator({
        rules: {
            CondominioObligatoria: function (input) {
                var result = true;
                if (input.is("[name=DIV_Descripcion]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=DIV_Ruta]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=DIV_Direccion]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            }
        },
        messages: {
            CondominioObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}