﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nueva Vendedor").width(700) : $('.k-window-title').text("Editar Vendedor").width(700);
        },
        onSave: function (e) {
            if (!VendedorValidator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function VendedorValidator() {
    var Validator = $("#VendedorContainer").kendoValidator({
        rules: {
            VendedorObligatoria: function (input) {
                var result = true;
                if (input.is("[name=NombreVendedor]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=ApellidoPaterno]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=ApellidoMaterno]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=Direccion]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                var expreg = new RegExp("^[0-9]{8}$");
                if (input.is("[name=DNI]")) {
                    if (input.val().trim() == "")
                        result = false;
                    else if (!expreg.test(input.val()))
                        result = false;

                }
                expreg = new RegExp("^[9]{1}[0-9]{8}$");
                if (input.is("[name=Telefono]")) {
                    if (input.val().trim() == "")
                        result = false;
                    else if (!expreg.test(input.val()))
                        result = false;
                }
                expreg = new RegExp("^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$");
                if (input.is("[name=CorreoElectronico]")) {
                    if (input.val().trim() == "")
                        result = false;
                    else if (!expreg.test(input.val()))
                        result = false;
                }
                return result;
            }
        },
        messages: {
            VendedorObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}