﻿var gridManager = function () {
    var IdGeneral = 0;
    var DetalleBanderaEdicion = 0;
    var fila;
    function requestOperation(e) {
        var gridDetalle = $("#gridDetalleRendicion").data("kendoGrid");
        switch (e.type) {
            case 'create':
                _alert("Se ha insertado correctamente", "success");
                if (e.response > 0) {
                    var newIdRepo = e.response;
                    var dataDetalles = gridDetalle.dataSource.data();
                    if (dataDetalles.length > 0) {
                        for (var i = 0; i < dataDetalles.length; i++) {
                            dataDetalles[i].IdRendicion = newIdRepo;
                        }
                        gridDetalle.dataSource.sync();
                    }
                    $("#RendicionGrid").data("kendoGrid").dataSource.read();
                }
                break;
            case 'update':
                if (e.response > 0) {
                    _alert("Se ha actualizado correctamente", "success");
                    var newIdRepo = e.response;
                    var dataDetalles = gridDetalle.dataSource.data();
                    if (dataDetalles.length > 0) {
                        for (var i = 0; i < dataDetalles.length; i++) {
                            dataDetalles[i].IdRendicion = newIdRepo;
                        }
                        gridDetalle.dataSource.sync();
                    }
                    $("#RendicionGrid").data("kendoGrid").dataSource.read();
                } 
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        ParaDetalle: function () {
            return {
                idRendicion:IdGeneral,
            }
        },
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nueva Rendicion").width(700) : $('.k-window-title').text("Editar Rendicion").width(700);
            DetalleBanderaEdicion = 0;
            $("#DetalleFondoFijoDetalle").hide()
            $("#EstadoRegistroDetalle").hide()
            var fechaCon = $("#REND_FecCon").data("kendoDatePicker");
            var fechaDoc = $("#REND_FecDoc").data("kendoDatePicker");
            fechaCon.value(new Date());
            fechaDoc.value(new Date());
            if (e.model.isNew()) {
                IdGeneral = 0;
                //var dropdownlist = $("#FF_Div").data("kendoDropDownList");
                //dropdownlist.enable(false);
                //dropdownlist = $("#DIV_Cod_Detalle").data("kendoDropDownList");
                //dropdownlist.enable(false);
                //dropdownlist = $("#Proveedor").data("kendoDropDownList");
                //dropdownlist.enable(false);
                $("#DetalleFondoFijo").hide()
                $("#EstadoRegistro").hide()
                $("#MontoTotal").hide()
                $(".k-grid-edit").hide()
                DetalleBanderaEdicion=0
            } else {
                IdGeneral=e.model.IdRendicion;
                $("#DetalleFondoFijo").show()
                $("#EstadoRegistro").show()
                $("#MontoTotal").show()
                $(".k-grid-edit").show()
                $("#gridDetalleRendicion").data("kendoGrid").dataSource.read()
            }
        },
        guardando: function (e) {
            //debugger
            if (!RendicionValidator()) {
                e.preventDefault();
            }

            $(".k-grid-edit").show()
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        },
        onClickAgregar: function (e) {
            if (DetalleBanderaEdicion == 0) {
                var gridDetalles = $("#gridDetalleRendicion").data("kendoGrid");
                var division = $("#DIV_Cod_Detalle").data("kendoDropDownList");
                var fechaCon = $("#REND_FecCon").data("kendoDatePicker");
                var fechaDoc = $("#REND_FecDoc").data("kendoDatePicker");
                var tipoDocumento = $("#REND_TipDoc").data("kendoNumericTextBox");
                var DocumentoReferencia = $("#REND_DocRef");
                var TipoImpuesto = $("#TIPIMP_TipImp").data("kendoDropDownList");
                var MontoGasto = $("#REND_MonGas").data("kendoNumericTextBox");
                var DescripcionGasto = $("#REND_Des");
                var TipodeOrden = $("#TIPORD_TipOrd").data("kendoDropDownList");
                var CuentaContable = $("#REND_CtaCon").data("kendoDropDownList");
                var Proveedor = $("#Proveedor").data("kendoDropDownList");
                if (!DetalleValidator()) {
                    e.preventDefault()
                } else {
                    gridDetalles.dataSource.add({
                        DIV_Cod: division.value(),
                        REND_FecCon: fechaCon.value(),
                        REND_FecDoc: fechaDoc.value(),
                        REND_TipDoc: tipoDocumento.value(),
                        REND_DocRef: DocumentoReferencia.val(),
                        TIPIMP_TipImp: TipoImpuesto.value(),
                        REND_MonGas: MontoGasto.value(),
                        REND_Des: DescripcionGasto.val(),
                        TIPORD_TipOrd: TipodeOrden.value(),
                        REND_CtaCon: CuentaContable.value(),
                        //REND_EstRegCtaCon :  ,
                        //IdRendicion :  ,
                        //FF_CtaCon :  ,
                        //FF_Div :  ,
                        //FF_Cod :  ,
                        PRO_Cod: Proveedor.value()
                    });
                    $(".k-grid-edit").hide()
                    cleanPersonaAfectada();
                }
            } else {
                if (!DetalleValidator()) {
                    e.preventDefault()
                }
                $("#AgregarDetalleRendicion")[0].innerText = "AGREGAR"
                var dato = $("#gridDetalleRendicion").data("kendoGrid").dataItem(fila);
                dato.DIV_Cod = $("#DIV_Cod_Detalle").data("kendoDropDownList").value();
                dato.REND_FecCon = $("#REND_FecCon").data("kendoDatePicker").value();
                dato.REND_FecDoc = $("#REND_FecDoc").data("kendoDatePicker").value();
                dato.REND_TipDoc = $("#REND_TipDoc").data("kendoNumericTextBox").value();
                dato.REND_DocRef = $("#REND_DocRef").val();
                dato.TIPIMP_TipImp = $("#TIPIMP_TipImp").data("kendoDropDownList").value();
                dato.REND_MonGas = $("#REND_MonGas").data("kendoNumericTextBox").value();
                dato.REND_Des = $("#REND_Des").val();
                dato.TIPORD_TipOrd = $("#TIPORD_TipOrd").data("kendoDropDownList").value();
                dato.REND_CtaCon = $("#REND_CtaCon").data("kendoDropDownList").value();
                dato.PRO_Cod = $("#Proveedor").data("kendoDropDownList").value();
                dato.dirty=true
                $("#gridDetalleRendicion").data("kendoGrid").saveChanges()
                setTimeout(function () {
                    $("#gridDetalleRendicion").data("kendoGrid").dataSource.read()
                }, 1000)
                $("#TabReportePrel").data("kendoTabStrip").select(2)
            }
            cleanPersonaAfectada();
        },
        onClickCancelar: function (e) {
            cleanPersonaAfectada();
            if (DetalleBanderaEdicion != 0) {

                $("#AgregarDetalleRendicion")[0].innerText = "AGREGAR"

            }
        },
        onChangeDetalles: function (e) {
            //var gridDetalle = $("#gridDetalleRendicion").data("kendoGrid");
            //gridDetalle.dataSource.sync();
            var codigo = $("#REND_Cod");
            var division = $("#DIV_Cod_Detalle").data("kendoDropDownList");
            var fechaCon = $("#REND_FecCon").data("kendoDatePicker");
            var fechaDoc = $("#REND_FecDoc").data("kendoDatePicker");
            var tipoDocumento = $("#REND_TipDoc").data("kendoNumericTextBox");
            var DocumentoReferencia = $("#REND_DocRef");
            var TipoImpuesto = $("#TIPIMP_TipImp").data("kendoDropDownList");
            var MontoGasto = $("#REND_MonGas").data("kendoNumericTextBox");
            var DescripcionGasto = $("#REND_Des");
            var TipodeOrden = $("#TIPORD_TipOrd").data("kendoDropDownList");
            var CuentaContable = $("#REND_CtaCon").data("kendoDropDownList");
            var Proveedor = $("#Proveedor").data("kendoDropDownList");


            codigo.val(e.model.REND_Cod);
            division.value(e.model.DIV_Cod);
            fechaCon.value(e.model.REND_FecCon);
            fechaDoc.value(e.model.REND_FecDoc);
            tipoDocumento.value(e.model.REND_TipDoc);
            DocumentoReferencia.val(e.model.REND_DocRef);
            TipoImpuesto.value(e.model.TIPIMP_TipImp);
            MontoGasto.value(e.model.REND_MonGas);
            DescripcionGasto.val(e.model.REND_Des);
            TipodeOrden.value(e.model.TIPORD_TipOrd);
            CuentaContable.value(e.model.REND_CtaCon);
            Proveedor.value(e.model.PRO_Cod);
            $("#AgregarDetalleRendicion")[0].innerText = "GUARDAR"
            $("#TabReportePrel").data("kendoTabStrip").select(1)
            DetalleBanderaEdicion = 1;
            fila = $("#gridDetalleRendicion tbody").find("tr[data-uid=\"" + e.model.uid + "\"]")
        },
        onRemoveDetalles: function (e) {
            if (e.model.IdPersona == 0) {
                var gridPersonas = $("gridDetalleRendicion").data("kendoGrid");
                var dataItem = gridPersonas.dataItem(e.row);
                gridPersonas.dataSource.remove(dataItem);
                e.preventDefault();
            }
        },
        onRequestEndDetalles: function (e) {
            switch (e.type) {
                case 'update':
                    $("#TotalRendicion").data("kendoNumericTextBox").value($("#TotalRendicion").data("kendoNumericTextBox").value() + e.response)
                    e.sender.read();
                    break;
                case 'destroy':
                    $("#TotalRendicion").data("kendoNumericTextBox").value($("#TotalRendicion").data("kendoNumericTextBox").value() + e.response)

                    e.sender.read();
                    break;
            }
        },
        finalizaAceptacionEntrevista: function () {
            $("<div id='confirm'></div>").kendoConfirm({
                title: "Finalizar Rendicion",
                content: "¿Esta seguro que desea ENVIAR todas las Rendiciones?",
                messages: {
                    okText: "Aceptar"
                }
            }).data("kendoConfirm")
                .open()
                .result
                .then(function () {
                    $.ajax({
                        method: "POST",
                        url: Url.EnviarRendicion,
                        data: {},
                    success: function (data) {
                        if (data != 0) {
                            ActualizarEntrevistaEmail();
                            alerta("success", "Se finalizó el envio de Rendiciones.");

                        }
                        else {
                            alerta("warning", "No se pudo finalizar la aceptacion de Entrevista.");
                        }
                    }
                     })
    });
    $("#confirm").parent().find('.k-window-titlebar.k-dialog-titlebar.k-header').addClass("mensaje-color");
}
    }
}();

// --->>>  VALIDATOR

function RendicionValidator() {
    var Validator = $("#cabeceraReporte").kendoValidator({
        rules: {
            //IdRendicion: function (input) {
            //    var result = true;
            //    if (input.is("[name=IdRendicion]")) {
            //        if (input.val().trim() == "")
            //            result = false;
            //    }
            //    return result;
            //},
            FechaRendicion: function (input) {
                var result = true;
                if (input.is("[name=FechaCreacionRendicion]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            },
            FechaEnvio: function (input) {
                var result = true;
                if (input.is("[name=FechaEnvioRendicion]")) {
                    if (input.val().trim() == null)
                        result = false;
                }
                return result;
            },
            FondoFijo: function (input) {
                var result = true;
                if (input.is("[name=FF_Cod]")) {
                    if (input.val() == 0)
                        result = false;
                }
                return result;
            }
        },
        messages: {
            //IdRendicion: "Campo Obligatorio",
            FechaRendicion: "Campo Obligatorio",
            FechaEnvio: "Campo Obligatorio",
            FondoFijo: "Campo Obligatorio"
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}

// ---->>>  DETALLE VALIDATOR
function DetalleValidator() {
    var Validator = $("#DetalleContainer").kendoValidator({
        rules: {
            //IdDetalle: function (input) {
            //    //var codigo = $("#REND_Cod");
            //    //var division = $("#DIV_Cod_Detalle").data("kendoDropDownList");
            //    //var fechaCon = $("#REND_FecCon").data("kendoDatePicker");
            //    //var fechaDoc = $("#REND_FecDoc").data("kendoDatePicker");
            //    //var tipoDocumento = $("#REND_TipDoc").data("kendoDropDownList");
            //    //var DocumentoReferencia = $("#REND_DocRef");
            //    //var TipoImpuesto = $("#TIPIMP_TipImp").data("kendoDropDownList");
            //    //var MontoGasto = $("#REND_MonGas").data("kendoNumericTextBox");
            //    //var DescripcionGasto = $("#REND_Des");
            //    //var TipodeOrden = $("#TIPORD_TipOrd").data("kendoDropDownList");
            //    //var CuentaContable = $("#REND_CtaCon").data("kendoDropDownList");
            //    var result = true;
            //    if (input.is("[name=REND_Cod]")) {
            //        if (input.val().trim() == "")
            //            result = false;
            //    }
            //    return result;
            //},
            DivisionDetalle: function (input) {
                var result = true;
                if (input.is("[name=DIV_Cod_Detalle]")) {
                    if (input.val() == 0)
                        result = false;
                }
                return result;
            },
            FechaConDetalle: function (input) {
                var result = true;
                if (input.is("[name=REND_FecCon]")) {
                    if ($("#REND_FecCon").data("kendoDatePicker").value() == null)
                        result = false;
                }
                return result;
            },
            FechaDocDetalle: function (input) {
                var result = true;
                if (input.is("[name=REND_FecDoc]")) {
                    if ($("#REND_FecDoc").data("kendoDatePicker").value() == null)
                        result = false;
                }
                return result;
            },
            TipoDocumentoDetalle: function (input) {
                var result = true;
                if (input.is("[name=REND_TipDoc]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            },
            DocumentoReferenciaDetalle: function (input) {
                var result = true;
                if (input.is("[name=REND_DocRef]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            },
            TipoImpuestoDetalle: function (input) {
                var result = true;
                if (input.is("[name=TIPIMP_TipImp]")) {
                    if (input.val() == 0)
                        result = false;
                }
                return result;
            },
            MontoGastadoDetalle: function (input) {
                var result = true;
                if (input.is("[name=REND_MonGas]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            },
            DescripcionDetalle: function (input) {
                var result = true;
                if (input.is("[name=REND_Des]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            },
            TipoOrdenDetalle: function (input) {
                var result = true;
                if (input.is("[name=TIPORD_TipOrd]")) {
                    if (input.val() == 0)
                        result = false;
                }
                return result;
            },
            CuentaContableDEtalle: function (input) {
                var result = true;
                if (input.is("[name=REND_CtaCon]")) {
                    if (input.val() == 0)
                        result = false;
                }
                return result;
            },
            ProveedorDetalle: function (input) {
                var result = true;
                if (input.is("[name=PRO_Cod]")) {
                    if (input.val() == 0)
                        result = false;
                }
                return result;
            }
        },
        messages: {
            //IdDetalle: "Campo Obligatorio",
            DivisionDetalle: "Campo Obligatorio",
            FechaConDetalle: "Campo Obligatorio",
            FechaDocDetalle: "Campo Obligatorio",
            TipoDocumentoDetalle: "Campo Obligatorio",
            DocumentoReferenciaDetalle: "Campo Obligatorio",
            TipoImpuestoDetalle: "Campo Obligatorio",
            MontoGastadoDetalle: "Campo Obligatorio",
            DescripcionDetalle: "Campo Obligatorio",
            TipoOrdenDetalle: "Campo Obligatorio",
            CuentaContableDEtalle: "Campo Obligatorio",
            ProveedorDetalle: "Campo Obligatorio"
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}

function cleanPersonaAfectada() {
    var codigo = $("#REND_Cod");
    var division = $("#DIV_Cod_Detalle").data("kendoDropDownList");
    var fechaCon = $("#REND_FecCon").data("kendoDatePicker");
    var fechaDoc = $("#REND_FecDoc").data("kendoDatePicker");
    var tipoDocumento = $("#REND_TipDoc").data("kendoNumericTextBox");
    var DocumentoReferencia = $("#REND_DocRef");
    var TipoImpuesto = $("#TIPIMP_TipImp").data("kendoDropDownList");
    var MontoGasto = $("#REND_MonGas").data("kendoNumericTextBox");
    var DescripcionGasto = $("#REND_Des");
    var TipodeOrden = $("#TIPORD_TipOrd").data("kendoDropDownList");
    var CuentaContable = $("#REND_CtaCon").data("kendoDropDownList");
    var Proveedor = $("#Proveedor").data("kendoDropDownList");


    codigo.val(null);
    division.value(null);
    fechaCon.value(new Date());
    fechaDoc.value(new Date());
    tipoDocumento.value(null);
    DocumentoReferencia.val(null);
    TipoImpuesto.value(null);
    MontoGasto.value(null);
    DescripcionGasto.val(null);
    TipodeOrden.value(null);
    CuentaContable.value(null);
    Proveedor.value(null);
    //NullPersonaInformation();
}