﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se ha registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nueva Cuenta Contable").width(700) : $('.k-window-title').text("Editar Cuenta Contable").width(700);
        },
        onSave: function (e) {
            if (!Validator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function Validator() {
    var Validator = $("#CuentasContainer").kendoValidator({
        rules: {
            CuentaContableObligatoria: function (input) {
                var result = true;
                if (input.is("[name=CTA_CtaCon]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                
                return result;
            },
            CuentaContableObligatoria: function (input) {
                var result = true;
                if (input.is("[name=CTA_Des]")) {
                    if (input.val().trim() == "")
                        result = false;
                }

                return result;
            },
            CuentaContableObligatoria: function (input) {
                var result = true;
                if (input.is("[name=CTA_EstRegA]")) {
                    if (input.val().trim() == "")
                        result = false;
                }

                return result;
            }
        },
        messages: {
            CuentaContableObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}