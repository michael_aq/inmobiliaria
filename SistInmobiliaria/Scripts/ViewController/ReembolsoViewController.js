﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nuevo Reembolso").width(700) : $('.k-window-title').text("Editar Reembolso").width(700);
        },
        onSave: function (e) {
            if (!ReembolsoValidator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        },
        reactivar: function (e) {
            $.ajax({
                method: "POST",
                url: Url.Reeditar,
                data: {
                    id: e
                },
                success: function (response) {
                    $("#ReembolsoGrid").data("kendoGrid").dataSource.read()
                    _alert("Se ha actualizado correctamente", "success");
                    console.log(response)
                }
            });
        },
        reembolsar: function (e) {
            $.ajax({
                method: "POST",
                url: Url.Reembolsar,
                data: {
                    id: e
                },
                success: function (response) {
                    $("#ReembolsoGrid").data("kendoGrid").dataSource.read()
                    _alert("Se ha actualizado correctamente", "success");
                    console.log(response)
                }
            });
        }
    }
}();

// --->>>  VALIDATOR

function ReembolsoValidator() {
    var Validator = $("#ReembolsoContainer").kendoValidator({
        rules: {
            ReembolsoObligatoria: function (input) {
                var result = true;
                if (input.is("[name=REM_Imp]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=REM_CtaCte]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=REM_FecEnv]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=REM_FecCon]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=IdRendicion]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=FF_CtaCon]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=FF_Div]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=FF_CtaCon]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=FF_Cod]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=FechaCrea]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                return result;
            }

        },
        messages: {
            ReembolsoObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}