﻿var gridManager = function () {
    function requestOperation(e) {
        switch (e.type) {
            case 'create':
                if (e.response.success) {

                    _alert("Se registrado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'update':
                if (e.response.success) {

                    _alert("Se ha actualizado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya existe", "error");
                }
                e.sender.read();
                break;
            case 'destroy':
                if (e.response.success) {
                    _alert("Se ha eliminado correctamente", "success");
                } else {
                    _alert("Opps, el registro ya esta siendo usado.", "error");
                }
                e.sender.read();
                break;
        }
    }
    return {
        // GridEvents
        onEdit: function (e) {
            e.model.isNew() ? $('.k-window-title').text("Registrar Nuevo Proveedor").width(700) : $('.k-window-title').text("Editar Proveedor").width(700);
        },
        onSave: function (e) {
            if (!ProveedorValidator()) {
                e.preventDefault();
            }
        },
        // DatasourceEvents
        onRequestEnd: function (e) {
            requestOperation(e);
        }
    }
}();

// --->>>  VALIDATOR

function ProveedorValidator() {
    var Validator = $("#ProveedorContainer").kendoValidator({
        rules: {
            ProveedorObligatoria: function (input) {
                var result = true;
                if (input.is("[name=PRO_Nom]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=PRO_Ruc]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=TIPCONFIS_TipConFis]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=PRO_FecIns]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=PRO_CtaCte]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                if (input.is("[name=PRO_Dir]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=PRO_DomFis]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=PRO_EstReg]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=PRO_EstReg]")) {
                    if (input.val().trim() == "")
                        result = false;
                } 
                if (input.is("[name=UsuarioCrea]")) {
                    if (input.val().trim() == "")
                        result = false;
                }
                return result;
            }

        },
        messages: {
            ProveedorObligatoria: "Campo Obligatorio",
        }
    }).data("kendoValidator");

    if (Validator.validate()) {
        return true;
    } else {
        return false;
    }
}