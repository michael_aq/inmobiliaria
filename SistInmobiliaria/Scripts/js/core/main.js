(function(){
    $("#simple-table").tablesorter();
    $("#guardarColumnas").click(function(){
        $("#table-report").slideDown();
    });

    // ------------------------------------------------- javascript for init
    $('.datetimepicker').datetimepicker({
        icons: {
        time: "tim-icons icon-watch-time",
        date: "tim-icons icon-calendar-60",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'tim-icons icon-minimal-left',
        next: 'tim-icons icon-minimal-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
        }
    });
})();

function alertaConfirmacion(type, message, time = 1500) {
        Swal.fire({
            position: 'top-end',
            type: type,
            title: message,
            showConfirmButton: false,
            timer: time
        })
}
function confirmation(title, _callback , decline) {
    Swal.fire({
        title: title,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
    }).then((result) => {
        if (result.value) {
            _callback();
        } else {
            decline();
        }
    })
}
function alerta(type, message) 
    {
        Swal.fire({
            position: 'top-end',
            type: type,
            title: message,
            showConfirmButton: true,
            //timer: time
        })
    }

$('.has-children').on('click', function () {
    $(this).children('div').slideToggle('slow', 'swing');
    $('.caret').toggleClass('open');
});

function limpiar() {
    $("div[id*=divFiltros]").find("input[type=text]").each(function () {
            $(this).val("");
        });

    $("div[id*=divFiltros]").find("input[data-role=dropdownlist]").each(function(){
        $(this).data("kendoDropDownList").value(null);
    });

     $("div[id*=divFiltros]").find("textarea").each(function(){
        $(this).val("");
    });
   return
}


$(document).ready(function () {
    //$("#divFiltros").hide();

    $("#cardIconFiltro, .close").on("click", function (e) {
        $("#divFiltros").toggle("slide");
        //$("#divFiltrosCS").toggle("slide");
    });
    $("#btnBuscar").on("click", function () {
        $("#divFiltros").css("display","block");
        //$("#divFiltros").toggle(300);
    });

});

function isNullOrWhiteSpace(str) {
    return (!str || str.length === 0 || /^\s*$/.test(str))
}