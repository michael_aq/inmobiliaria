﻿using System.Web.Optimization;

namespace SistInmobiliaria
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region JS
                bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/js/jquery-{version}.js"));

                bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                            "~/Scripts/js/jquery.validate*"));

                // Use the development version of Modernizr to develop with and learn from. Then, when you're
                // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
                bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/js/modernizr-*"));

                bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                          "~/Scripts/js/bootstrap.js",
                          "~/Scripts/js/respond.js"));

                bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                      "~/Scripts/kendo/kendo.all.min.js",
                      "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                      "~/Scripts/kendo/cultures/kendo.culture.es-PE.min.js",
                      "~/Scripts/kendo/messages/kendo.messages.es-PE.min.js"));
            #endregion


            #region CSS
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css/Site.css"));

                bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                    "~/Content/kendo/kendo.common-boostrap.min.css",
                    "~/Content/kendo/kendo.boostrap.min.css"));
            #endregion

        }
    }
}
