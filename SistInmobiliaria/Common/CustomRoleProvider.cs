﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Infrastructure.Repositories;

namespace SistInmobiliaria.UI.Common
{
    public class CustomRoleProvider : RoleProvider
    {

        //TODO AVERIGUAR COMO INJECTAR LA DEPENDENCIA EN UN PROVIDER
        //private IUserDataServices _userDataServices = new UserDataServices();
        private IUserRepository _userRepository = new UserRepository();
        public override string[] GetRolesForUser(string username)
        {
            IList<string> listAccess = _userRepository.GetRolesByUser(username);
            return listAccess.ToArray();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            // IList<string> listAccess = _userDataServices.GetRolesByCode(username);
            // return listAccess.Contains(roleName);
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}