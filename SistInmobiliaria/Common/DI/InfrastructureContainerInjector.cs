﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity.Extension;
using Unity;
using System.Configuration;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Infrastructure.Repositories;

namespace SistInmobiliaria.UI.Common.DI
{
    public class InfrastructureContainerInjector : UnityContainerExtension
    {
        protected override void Initialize()

        {
            // General
            Container.RegisterType<IUserRepository, UserRepository>();
            Container.RegisterType<IArchivoRepository, ArchivoRepository>();

            // Modulo Reservas
            Container.RegisterType<IClienteRepository, ClienteRepository>();
            //Container.RegisterType<ICondominioRepository, CondominioRepository>();
            Container.RegisterType<IDepartamentoRepository, DepartamentoRepository>();
            Container.RegisterType<IReservaRepository, ReservaRepository>();
            Container.RegisterType<IVendedorRepository, VendedorRepository>();

            // Módulo Fondos Fijos
            Container.RegisterType<IFondoFijoRepository, FondoFijoRepository>();  
            Container.RegisterType<IReembolsoRepository, ReembolsoRepository>();
            Container.RegisterType<IRendicionCABRepository, RendicionCABRepository>();
            Container.RegisterType<IRendicionDETRepository, RendicionDETRepository>();
            Container.RegisterType<IProveedorRepository, ProveedorRepository>();
            Container.RegisterType<ICuentasContablesRepository, CuentasContablesRepository>();

            // tablas generales y referenciales
            Container.RegisterType<IEstadoRegistroRepository, EstadoRegistroRepository>();
            Container.RegisterType<IDivisionRepository, DivisionRepository>();
            Container.RegisterType<ITipoCondicionFiscalRepository, TipoCondicionFiscalRepository>();
            Container.RegisterType<ITipoImpuestoRepository, TipoImpuestoRepository>();
            Container.RegisterType<ITipoOrdenRepository, TipoOrdenRepository>();



        }
    }
}