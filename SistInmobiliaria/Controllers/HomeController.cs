﻿using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}