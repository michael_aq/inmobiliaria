﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class DropDownsController : Controller
    {
        private readonly IClienteRepository _IClienteRepository;
        private readonly IDepartamentoRepository _IDepartamentoRepository;
        //private readonly ICondominioRepository _ICondominioRepository;
        private readonly IVendedorRepository _IVendedorRepository;
        private readonly IEstadoRegistroRepository _IEstadoRegistroRepository;
        private readonly IDivisionRepository _IDivisionRepository;
        private readonly ITipoCondicionFiscalRepository _ITipoCondicionFiscalRepository;
        private readonly ITipoImpuestoRepository _ITipoImpuestoRepository;
        private readonly ITipoOrdenRepository _ITipoOrdenRepository;
        private readonly ICuentasContablesRepository _ICuentasContablesRepository;
        private readonly IFondoFijoRepository _IFondoFijoRepository;
        private readonly IProveedorRepository _IProveedorRepository;
        public DropDownsController(//ICondominioRepository ICondominioRepository,
                                     IClienteRepository IClienteRepository
                                    , IDepartamentoRepository IDepartamentoRepository
                                    , IVendedorRepository IVendedorRepository
                                    , IEstadoRegistroRepository IEstadoRegistroRepository
                                    , IDivisionRepository IDivisionRepository
                                    , ITipoCondicionFiscalRepository ITipoCondicionFiscalRepository
                                    , ITipoImpuestoRepository ITipoImpuestoRepository
                                    , ITipoOrdenRepository ITipoOrdenRepository
                                    , ICuentasContablesRepository ICuentasContablesRepository
                                    , IFondoFijoRepository IFondoFijoRepository
                                    , IProveedorRepository IProveedorRepository)
        {
            //_ICondominioRepository = ICondominioRepository;
            _IDepartamentoRepository = IDepartamentoRepository;
            _IClienteRepository = IClienteRepository;
            _IVendedorRepository = IVendedorRepository;
            _IEstadoRegistroRepository = IEstadoRegistroRepository;
            _IDivisionRepository = IDivisionRepository;
            _ITipoCondicionFiscalRepository = ITipoCondicionFiscalRepository;
            _ITipoImpuestoRepository = ITipoImpuestoRepository;
            _ITipoOrdenRepository = ITipoOrdenRepository;
            _ICuentasContablesRepository = ICuentasContablesRepository;
            _IFondoFijoRepository = IFondoFijoRepository;
            _IProveedorRepository = IProveedorRepository;
        }
        /*public ActionResult GetAllCondominiosDrop()
        {
            try
            {
                var list = _ICondominioRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<CondominioVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }*/
        public ActionResult GetAllClientesDrop()
        {
            try
            {
                var list = _IClienteRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<ClienteVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllDepartamentosDrop(int tipo, int id)
        {
            try
            {
                
                var list = _IDepartamentoRepository.GetAll();
                if (tipo == 1) {
                     list =list.Where(p => p.Estado == '1' || p.IdDepartamento == id).ToList();
                }
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<DepartamentoVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllVendedoresDrop()
        {
            try
            {
                var list = _IVendedorRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<VendedorVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllEstadoRegistroDrop(string tipo)
        {
            try
            {
                var list = _IEstadoRegistroRepository.GetSegunTipo(tipo);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                var list = new List<EstadoRegistroVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllDivisionDrop()
        {
            try
            {
                var list = _IDivisionRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<DivisionVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllTipoCondicionFiscalDrop()
        {
            try
            {
                var list = _ITipoCondicionFiscalRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<TipoCondicionFiscalVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllProveedorDrop()
        {
            try
            {
                var list = _IProveedorRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<ProveedorVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        } 
        public ActionResult GetAllTipoImpuestoDrop()
        {
            try
            {
                var list = _ITipoImpuestoRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<TipoImpuestoVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllTipoOrdenDrop()
        {
            try
            {
                var list = _ITipoOrdenRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<TipoOrdenVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllCuentasContablesDrop()
        {
            try
            {
                var list = _ICuentasContablesRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<CuentasContablesVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllFondosFijosDrop()
        {
            try
            {
                var list = _IFondoFijoRepository.GetAll();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var list = new List<FondoFijoVM>();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

    }
}