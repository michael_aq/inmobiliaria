﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class EstadoRegistroController : Controller
    {
        // GET: EstadoRegistro

        private readonly IEstadoRegistroRepository _IEstadoRegistroRepository;
        public EstadoRegistroController(IEstadoRegistroRepository IEstadoRegistroRepository)
        {
            _IEstadoRegistroRepository = IEstadoRegistroRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllEstadoRegistro([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IEstadoRegistroRepository.GetSegunTipo("XXX");
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<EstadoRegistroVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
       
    }


}
