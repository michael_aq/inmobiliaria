﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class FondoFijoController : Controller
    {
        private readonly IFondoFijoRepository _IFondoFijoRepository;
        public FondoFijoController(IFondoFijoRepository IFondoFijoRepository)
        {
            _IFondoFijoRepository = IFondoFijoRepository;
        }
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult GetAllFondosFijos([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IFondoFijoRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<FondoFijoVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertFondoFijo([DataSourceRequest] DataSourceRequest request, FondoFijoVM model)
        {
            try
            {
                var result = _IFondoFijoRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateFondoFijo([DataSourceRequest] DataSourceRequest request, FondoFijoVM model)
        {
            try
            {
                var result = _IFondoFijoRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteFondoFijo([DataSourceRequest] DataSourceRequest request, FondoFijoVM model)
        {
            try
            {
                var result = _IFondoFijoRepository.Delete(model.FF_Cod);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }
}