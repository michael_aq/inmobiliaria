﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class ProveedorController : Controller
    {
        // GET: 

        private readonly IProveedorRepository _IProveedorRepository;
        public ProveedorController(IProveedorRepository IProveedorRepository)
        {
            _IProveedorRepository = IProveedorRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllProveedor([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IProveedorRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<ProveedorVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertProveedor([DataSourceRequest] DataSourceRequest request, ProveedorVM model)
        {
            try
            {
                var result = _IProveedorRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateProveedor([DataSourceRequest] DataSourceRequest request, ProveedorVM model)
        {
            try
            {
                var result = _IProveedorRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteProveedor([DataSourceRequest] DataSourceRequest request, ProveedorVM model)
        {
            try
            {
                var result = _IProveedorRepository.Delete(model.PRO_Cod);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }


}
