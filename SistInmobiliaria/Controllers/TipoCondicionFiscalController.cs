﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class TipoCondicionFiscalController : Controller
    {
        // GET: TipoCondicionFiscal

        private readonly ITipoCondicionFiscalRepository _ITipoCondicionFiscalRepository;
        public TipoCondicionFiscalController(ITipoCondicionFiscalRepository ITipoCondicionFiscalRepository)
        {
            _ITipoCondicionFiscalRepository = ITipoCondicionFiscalRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllTipoCondicionFiscal([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _ITipoCondicionFiscalRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<TipoCondicionFiscalVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
       
    }


}
