﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class DivisionController : Controller
    {
        // GET: Division

        private readonly IDivisionRepository _IDivisionRepository;
        public DivisionController(IDivisionRepository IDivisionRepository)
        {
            _IDivisionRepository = IDivisionRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllDivision([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IDivisionRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<DivisionVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertDivision([DataSourceRequest] DataSourceRequest request, DivisionVM model)
        {
            try
            {
                var result = _IDivisionRepository.Insert(model);
                return Json(new { success = true, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public ActionResult UpdateDivision([DataSourceRequest] DataSourceRequest request, DivisionVM model)
        {
            try
            {
                var result = _IDivisionRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteDivision([DataSourceRequest] DataSourceRequest request, DivisionVM model)
        {
            try
            {
                var result = _IDivisionRepository.Delete(model.DIV_Codigo);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }


}
