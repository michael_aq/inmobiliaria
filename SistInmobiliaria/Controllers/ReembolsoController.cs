﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class ReembolsoController : Controller
    {
        // GET: Reembolso
        private readonly IReembolsoRepository _IReembolsoRepository;
        public ReembolsoController(IReembolsoRepository IReembolsoRepository)
        {
            _IReembolsoRepository = IReembolsoRepository;
        }
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult GetAllReembolsos([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IReembolsoRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<ReembolsoVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateReembolso([DataSourceRequest] DataSourceRequest request, ReembolsoVM model)
        {
            try
            {
                var result = _IReembolsoRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteReembolso([DataSourceRequest] DataSourceRequest request, ReembolsoVM model)
        {
            try
            {
                var result = _IReembolsoRepository.Delete(model.REM_Cod);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        [HttpPost]
        public ActionResult ActivarEdicion(int id)
        {
            try
            {
                _IReembolsoRepository.Reeditar(id);
                return Json( true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }
        [HttpPost]
        public ActionResult Reembolsar(int id)
        {
            try
            {
               _IReembolsoRepository.Reembolsar(id);
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }
    }
}