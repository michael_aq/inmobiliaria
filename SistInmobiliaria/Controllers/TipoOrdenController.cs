﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class TipoOrdenController : Controller
    {
        // GET: TipoOrden

        private readonly ITipoOrdenRepository _ITipoOrdenRepository;
        public TipoOrdenController(ITipoOrdenRepository ITipoOrdenRepository)
        {
            _ITipoOrdenRepository = ITipoOrdenRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllTipoOrden([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _ITipoOrdenRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<TipoOrdenVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
       
    }


}
