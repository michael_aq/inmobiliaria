﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class DepartamentoController : Controller
    {
        private readonly IDepartamentoRepository _IDepartamentoRepository;
        public DepartamentoController(IDepartamentoRepository IDepartamentoRepository)
        {
            _IDepartamentoRepository = IDepartamentoRepository;
        }
        // GET: Departamento
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllDepartamentos([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IDepartamentoRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<DepartamentoVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertDepartamento([DataSourceRequest] DataSourceRequest request, DepartamentoVM model)
        {
            try
            {
                var result = _IDepartamentoRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateDepartamento([DataSourceRequest] DataSourceRequest request, DepartamentoVM model)
        {
            try
            {
                var result = _IDepartamentoRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteDepartamento([DataSourceRequest] DataSourceRequest request, DepartamentoVM model)
        {
            try
            {
                var result = _IDepartamentoRepository.Delete(model.IdDepartamento);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }
}