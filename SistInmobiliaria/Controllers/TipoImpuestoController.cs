﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class TipoImpuestoController : Controller
    {
        // GET: TipoImpuesto

        private readonly ITipoImpuestoRepository _ITipoImpuestoRepository;
        public TipoImpuestoController(ITipoImpuestoRepository ITipoImpuestoRepository)
        {
            _ITipoImpuestoRepository = ITipoImpuestoRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllTipoImpuesto([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _ITipoImpuestoRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<TipoImpuestoVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
       
    }


}
