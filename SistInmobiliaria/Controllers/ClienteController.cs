﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteRepository _IClienteRepository;
        public ClienteController(IClienteRepository IClienteRepository)
        {
            _IClienteRepository = IClienteRepository;
        }
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult GetAllClientes([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IClienteRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<ClienteVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertCliente([DataSourceRequest] DataSourceRequest request, ClienteVM model)
        {
            try
            {
                var result = _IClienteRepository.Insert(model);
                return Json(new { success = result >-1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateCliente([DataSourceRequest] DataSourceRequest request, ClienteVM model)
        {
            try
            {
                var result = _IClienteRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteCliente([DataSourceRequest] DataSourceRequest request, ClienteVM model)
        {
            try
            {
                var result = _IClienteRepository.Delete(model.IdCliente);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }
}