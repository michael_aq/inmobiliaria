﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class ReservaController : Controller
    {
        private readonly IReservaRepository _IReservaRepository;
        public ReservaController(IReservaRepository IReservaRepository)
        {
            _IReservaRepository = IReservaRepository;
        }
        // GET: Reserva
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllReservas([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IReservaRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<ReservaVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertReserva([DataSourceRequest] DataSourceRequest request, ReservaVM model)
        {
            try
            {
                var result = _IReservaRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateReserva([DataSourceRequest] DataSourceRequest request, ReservaVM model)
        {
            try
            {
                var result = _IReservaRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteReserva([DataSourceRequest] DataSourceRequest request, ReservaVM model)
        {
            try
            {
                var result = _IReservaRepository.Delete(model.IdReserva);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }
}