﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class VendedorController : Controller
    {
        private readonly IVendedorRepository _IVendedorRepository;
        public VendedorController(IVendedorRepository IVendedorRepository)
        {
            _IVendedorRepository = IVendedorRepository;
        }
        // GET: Vendedor
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllVendedores([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IVendedorRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<VendedorVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertVendedor([DataSourceRequest] DataSourceRequest request, VendedorVM model)
        {
            try
            {
                var result = _IVendedorRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateVendedor([DataSourceRequest] DataSourceRequest request, VendedorVM model)
        {
            try
            {
                var result = _IVendedorRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteVendedor([DataSourceRequest] DataSourceRequest request, VendedorVM model)
        {
            try
            {
                var result = _IVendedorRepository.Delete(model.IdVendedor);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }
}