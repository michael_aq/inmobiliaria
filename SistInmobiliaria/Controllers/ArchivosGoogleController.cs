﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using System.IO;
using Google.Apis.Drive.v3;

namespace SistInmobiliaria.Controllers
{
    public class ArchivosGoogleController : Controller
    {
        static string[] Scopes = { DriveService.Scope.DriveReadonly };
        static string ApplicationName = "Drive API .NET Quickstart";
        private Google.Apis.Drive.v3.DriveService GetService()
        {
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Drive API service.
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            return service;
        }


        private void FileUploadInFolder(/*string folderId,*/ HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                Google.Apis.Drive.v3.DriveService service = GetService();

                string path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/GoogleDriveFiles"),
                Path.GetFileName(file.FileName));
                file.SaveAs(path);

                var FileMetaData = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = Path.GetFileName(file.FileName),
                    MimeType = "image/jpeg",
                   // Parents = new List<string>
                   //{
                   //    folderId
                   //}
                };

                Google.Apis.Drive.v3.FilesResource.CreateMediaUpload request;
                using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open))
                {
                    request = service.Files.Create(FileMetaData, stream, FileMetaData.MimeType);
                    request.Fields = "id";
                    request.Upload();
                }
                var file1 = request.ResponseBody;
            }
        }
        public ActionResult SubirImagenes(IEnumerable<HttpPostedFileBase> fotos)
        {
            foreach(var temp in fotos)
            {
                string ruta = System.Web.HttpContext.Current.Server.MapPath("~/GoogleDriveFiles");
                var physicalPath = Path.Combine(ruta, temp.FileName);

                if (!Directory.Exists(ruta))
                {
                    DirectoryInfo directorio = Directory.CreateDirectory(ruta);
                }

                temp.SaveAs(physicalPath);
                FileUploadInFolder(/*"1C9LWDHHMkqvbDgBQRlgt6bvlK8rFuspg",*/ temp);
            }
            return Json(new { success = true });
        }
    }
}