﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class CondominioController : Controller
    {
        private readonly ICondominioRepository _ICondominioRepository;
        public CondominioController(ICondominioRepository ICondominioRepository)
        {
            _ICondominioRepository = ICondominioRepository;
        }
        // GET: Condominio
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllCondominios([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _ICondominioRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<CondominioVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertCondominio([DataSourceRequest] DataSourceRequest request, CondominioVM model)
        {
            try
            {
                var result = _ICondominioRepository.Insert(model);
                return Json(new { success =true, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateCondominio([DataSourceRequest] DataSourceRequest request, CondominioVM model)
        {
            try
            {
                var result = _ICondominioRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteCondominio([DataSourceRequest] DataSourceRequest request, CondominioVM model)
        {
            try
            {
                var result = _ICondominioRepository.Delete(model.IdCondominio);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }
}