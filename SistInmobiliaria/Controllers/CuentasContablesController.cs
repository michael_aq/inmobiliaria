﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class CuentasContablesController : Controller
    {
        // GET: CuentasContables

        private readonly ICuentasContablesRepository _ICuentasContablesRepository;
        public CuentasContablesController(ICuentasContablesRepository ICuentasContablesRepository)
        {
            _ICuentasContablesRepository = ICuentasContablesRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // aqui
        public ActionResult GetAllCuentasContables([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _ICuentasContablesRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<CuentasContablesVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertCuentaContable([DataSourceRequest] DataSourceRequest request, CuentasContablesVM model)
        {
            try
            {
                var result = _ICuentasContablesRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateCuentaContable([DataSourceRequest] DataSourceRequest request, CuentasContablesVM model)
        {
            try
            {
                var result = _ICuentasContablesRepository.update(model);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteCuentaContable([DataSourceRequest] DataSourceRequest request, CuentasContablesVM model)
        {
            try
            {
                var result = _ICuentasContablesRepository.Delete(model.CTA_CtaCon);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
    }


}
