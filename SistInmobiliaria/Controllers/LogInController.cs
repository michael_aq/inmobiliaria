﻿using Inmobiliaria.Core.Common;
using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SistInmobiliaria.Controllers
{
    public class LogInController : Controller
    {
        private readonly IUserRepository _userRepository;
        // GET: LogIn
        public LogInController(IUserRepository injUserRepository)
        {
            _userRepository = injUserRepository;
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        [HttpGet]
        public ActionResult LogIn(string returnUrl)
        {
            var model = new UserResultVM
            {
                ReturnUrl = returnUrl
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult LogIn(UserResultVM model)
        {
            try
            {

                var userDA = _userRepository.GetUserByUserName(model.CodUserName).FirstOrDefault();

                if (userDA == null)
                {
                    ModelState.AddModelError("", "usuario inválido");
                    return View();
                }

                if (userDA.EsUsuarioAD)
                {
                    //validación contra AD
                    var authAd = Membership.ValidateUser(model.CodUserName, model.Contrasenia);
                    if (!authAd)
                    {
                        ModelState.AddModelError("", "Usuario inválido para AD");
                        return View();
                    }
                }
                else
                {
                    var encodePass = Util.EncodePassword(model.Contrasenia);
                    //validacion contra base
                    bool authPass = _userRepository.ValidateUserPassword(model.CodUserName, encodePass);
                    if (!authPass)
                    {
                        ModelState.AddModelError("", "Usuario y/o clave inválida ");
                        return View();
                    }

                }

                var identity = new ClaimsIdentity();

                identity = new ClaimsIdentity(new[]
                {
                        new Claim(ClaimTypes.Name, userDA.CodUserName),
                        new Claim(ClaimTypes.NameIdentifier, userDA.CodigoTrabajador + ""),
                        new Claim(ClaimTypes.GivenName, userDA.NombreTrabajador),

                }, "ApplicationCookie");


                var ctx = Request.GetOwinContext();

                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
                return Redirect(GetRedirectUrl(model.ReturnUrl));

            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error  " + e.Message);
                return View();
            }
        }
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.Session.Clear();
            HttpContext.Session.Abandon();
            HttpContext.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("LogIn", "LogIn");
        }
        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl))
            {
                return Url.Action("Index", "Home");
            }

            return returnUrl;
        }
    }
}