﻿using Inmobiliaria.Core.IRepositories;
using Inmobiliaria.Core.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistInmobiliaria.Controllers
{
    public class RendicionController : Controller
    {
        // GET: RendicionCAB
        private readonly IRendicionCABRepository _IRendicionCABRepository;
        private readonly IRendicionDETRepository _IRendicionDETRepository;
        private readonly IReembolsoRepository _IReembolsoRepository;
        public RendicionController(IRendicionCABRepository IRendicionCABRepository
                                , IRendicionDETRepository IRendicionDETRepository
                                , IReembolsoRepository IReembolsoRepository)
        {
            _IRendicionCABRepository = IRendicionCABRepository;
            _IRendicionDETRepository = IRendicionDETRepository;
            _IReembolsoRepository = IReembolsoRepository;
        }
        public ActionResult Index()
        {

            return View();
        }
        #region CABECERA
        public ActionResult GetAllRendiciones([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var list = _IRendicionCABRepository.GetAll();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var list = new List<RendicionCABVM>();
                return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InsertRendicion([DataSourceRequest] DataSourceRequest request, RendicionCABVM model)
        {
            try
            {
                var result = _IRendicionCABRepository.Insert(model);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        public ActionResult UpdateRendicion([DataSourceRequest] DataSourceRequest request, RendicionCABVM model)
        {
            try
            {
                var result = _IRendicionCABRepository.update(model);
                return Json(model.IdRendicion, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        public JsonResult DeleteRendicion([DataSourceRequest] DataSourceRequest request, RendicionCABVM model)
        {
            try
            {
                var result = _IRendicionCABRepository.Delete(model.IdRendicion);
                return Json(new { success = result == 1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }
        }
        #endregion CABECERA
        #region DETALLES
        public ActionResult InsertDetalle([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RendicionDETVM> detalles)
        {
            foreach (var detalle in detalles)
            {
                _IRendicionDETRepository.Insert(detalle);
            }
            return Json(new[] { detalles }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDetalle([DataSourceRequest]DataSourceRequest request,  [Bind(Prefix = "models")]IEnumerable<RendicionDETVM> detalles)
        {
            double reduccion = 0;
            foreach (var detalle in detalles)
            {
                reduccion+=_IRendicionDETRepository.actualizacion(detalle).Value;
            }
            return Json(reduccion, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteDetalle([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RendicionDETVM> detalles)
        {
            double reducion= 0;
            foreach (var detalle in detalles)
            {
                reducion+=_IRendicionDETRepository.eliminacion(detalle.REND_Cod).Value;
            }
            return Json(reducion, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDetallesbyCabecera([DataSourceRequest]DataSourceRequest request, int idRendicion)
        {
            var res = _IRendicionDETRepository.GetAllByCabecera(idRendicion);
            return Json(res.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion DETALLES
        #region REEMBOLSO
        [HttpPost]
        public ActionResult InsertReembolso()
        {
            try
            {
                ReembolsoVM model = new ReembolsoVM();
                var result = _IReembolsoRepository.Insert(model);
                return Json(new { success = result > -1 ? true : false, errorMessage = "" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, errorMessage = e.Message });
            }

        }
        #endregion
    }
}